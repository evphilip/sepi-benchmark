# SEPI benchmark

Benchmarks for different SEPI versions

## Download and curate 

To download a list of models for BioModels and clean them: 

* change the classname in line 4 of the script to the corresponding class. 
* change the list in line 5 of the script to the corresponding model numbers. 
* run ``` ./download_and_curate.sh ```

## Search sepi between a list of models

To compare models of the same class: 

* set variables at the beginning of the file, for example: 
```
classname="ca_oscillations"
classall="39 43 44 45 58 98 115 117 122 145 166"
classpair="98-115 122-45"
timeout="120"
merge="no"
reductions="yes"
bottom="yes"
sepilimit=200
debug="yes"
```
* comment one of the last two lines depending on what you want to test. For example:
```
test_all
#test_pair
```
* run ``` ./search_sepi.sh ```

(it takes a lot of time)

## Draw graph of relations between models 

``` python3 draw_graph.py inputFile.txt outputFile.jpg```

with inputFile.txt the output of the script search_sepi.sh and outpuFile.jpg the name you want to give to your output file.

Options:

``` 
python3 draw_graph.py inputFile.txt outputFile.jpg -t True -c True -s True
-t draw timeout
-c distinct colors for each class
-s write sepi count
```

## Give the statistics of sepis between a list of models

Use the script count_sepi.py :

``` python3 count_sepi.py inputFile.txt ```

with inputFile.txt the output of the script search_sepi.sh

Returns (in the shell) some statistics on the set of all sepis between a list of models (nb of tested pairs, nb of sepis, mean, ...)

!! Careful : the number of sepis between two graphs is bounded by 200 --> alter the total and the mean

Moreover, testing each pair with both orders implies that at least half of the tested pairs have 0 sepis if there are no isomorphisms.

## Create a table with all sepis between two graphs

Use the script table_reductions.py, for example:

``` python3 table_reductions.py mapk/BIOMD0000000029.bc mapk/BIOMD0000000027.bc outputFile.csv ```

With outputFile.csv the name you want to give to your output file.

## Class from article

MAPK
```
9 10 11 26 27 28 29 30 31 49 146
```

Circadian clock 
```
21 22 34 55 73 74 78 83 89 170 171
```

Calcium oscillations
```
39 43 44 45 58 98 115 117 122 145 166
```

Cell cycle
```
7 8 56 109 111 144 168 169 196
```

## Class from BioModels 

(only list of models containing the classname in their title in Biocham)

MAPK 
```
9 10 11 14 19 26 27 28 29 30 31 49 146 399 430 431 432 433 440 441 442 443 444 491 492 659 664 666 667
```

Circadian clock 
```
16 22 36 55 73 74 78 83 89 160 171 185 214 216 273 295 411 412 437 445 564 577 631
```

Calcium oscillations
```
39 43 44 45 47 58 114 115 117 145 184 329
```

## Ajouts Eva début août

- dans tests_new_models, fichiers intra_mapk_pattern_neigh.txt et .jpg : recherche de sepis avec la merge restriction neighbours, sur les graphes reduced obtenus après pattern_reduction (réductions maximales, avec r_1, r_2 et ep à maybe, enzyme à yes, à partir des fichiers .bc avec la nouvelle curation)

