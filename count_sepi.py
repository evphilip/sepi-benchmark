#!/usr/bin/env python3
"""
Read output of search_sepi.sh and compute some statistics
"""

import argparse
import logging
import re
from statistics import mean, median

# Variables to set manually

max_red = "200" # maximal number of reductions (max_reductions with the script search_sepi.sh)

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.WARNING)

def parseArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("inputFile", help="inputFile", type=str)
    args = parser.parse_args()
    return args

def read_ouput(inputFile):
    """Read a file and return a list with the number of sepis
    Input:
        inputFile (str): input file name
    Output:
        (list): list with the number of sepis
    """
    logging.debug('Start reading inputFile')
    nb_to = 0 #nb of timeouts
    nb_no = 0 #nb of pairs with no sepi
    nb_max = 0 #nb of pairs with more sepis than the maximal number 
    sepi_nbs = [] #list of the number of sepis for pairs with at least one sepi and less than max_red
    with open(inputFile,'r') as data:
        for line in data.readlines():
            try:
                m = re.match(r"(.*) (.*) (.*)", line)
                nb_sepi = m.group(3)
            except AttributeError:
                nb_sepi = "0"
            if nb_sepi == "-1":
                nb_to+=1
            elif nb_sepi == "0":
                nb_no+=1
            elif nb_sepi == max_red:
                nb_max+=1
            else:
                sepi_nbs.append(int(nb_sepi))
    logging.debug('List constructed')
    return [nb_to, nb_no, nb_max, sepi_nbs]

def compute_stat(sepi_list):
    """Take a list with the number of sepis and return some stats about it
    Input:
        sepi_list (list): list with the number of sepis
    Output:
        nb_couple (int)
        mean_sepi (int)
        med_sepi (int)
        max_sepi (int)
        min_sepi (int)
    """
    nb_couple = len(sepi_list[3])+sepi_list[0]+sepi_list[1]+sepi_list[2]
    nb_to = sepi_list[0]
    nb_no = sepi_list[1]
    nb_sepi = len(sepi_list[3])+sepi_list[2]
    nb_max = sepi_list[2]
    mean_sepi = mean(sepi_list[3])
    med_sepi = median(sepi_list[3])
    if sepi_list[2]==0:
        max_sepi = max(sepi_list[3])
    else:
        max_sepi = max_red
    min_sepi = min(sepi_list[3])
    return [nb_couple, nb_to, nb_no, nb_sepi, nb_max, mean_sepi, med_sepi, max_sepi, min_sepi]


if __name__ == "__main__":
    logging.debug('Start program')
    args = parseArguments()
    sepi_list = read_ouput(args.inputFile)
    logging.debug('List of sepis: {}'.format(sepi_list))
    stats = compute_stat(sepi_list)
    print('number of tests between models: {}'.format(stats[0]))
    print('number of pairs with more than {} sepis: {} ({} %)'.format(max_red, stats[4], stats[4]*100/stats[0]))
    print('number of pairs with number of sepis between 1 and {}: {} ({} %)'.format(max_red, stats[3]-stats[4], (stats[3]-stats[4])*100/stats[0]))
    print('number of pairs with sepis: {} ({} %)'.format(stats[3], stats[3]*100/stats[0]))
    print('number of pairs with no sepis: {} ({} %)'.format(stats[2], stats[2]*100/stats[0]))
    print('number of timeouts: {} ({} %)'.format(stats[1], stats[1]*100/stats[0]))
    print('mean nb sepis when greater than 1 and smaller than {}: {}'.format(max_red, stats[5]))
    print('median nb sepis when greater than 1 and smaller than {}: {}'.format(max_red, stats[6]))
    print('max nb sepis: {}'.format(stats[7]))
    print('min nb sepis: {}'.format(stats[8]))



