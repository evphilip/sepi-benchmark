#!/bin/bash
set -e

classname="mapk"
class="9 10 11 26 27 28 29 30 31 49 146"

mkdir -p $classname
cd $classname

for i in $class ; do
    filename=`printf BIOMD%.10d $i`
    echo "load_sbml('$filename.xml'). export_ode('$filename.ode')." \
       "load_reactions_from_ode('$filename.ode')." \
       "export_biocham('$filename.bc')." | biocham
done
