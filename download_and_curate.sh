#!/bin/bash
set -e

classname="all"
class="7 8 9 10 11 21 22 26 27 28 29 30 31 34 39 43 44 45 49 55 58 73 74 78 83 89 98 109 111 115 117 122 144 145 146 166 168 169 170 171"
class="111"

mkdir -p $classname
cd $classname

for i in $class ; do
    filename=`printf BIOMD%.10d $i`
    url="https://www.ebi.ac.uk/biomodels-main/download?mid=$filename"
    if ! curl -fsLSo $filename.xml $url || grep -q '<!DOCTYPE' $filename.xml; then
        rm -f $filename.xml
        break
    fi
    echo "load_sbml('$filename.xml'). export_ode('$filename.ode')." \
       "load_reactions_from_ode('$filename.ode')." \
       "export_biocham('${filename}_new.bc')." | biocham
done
