#!/bin/bash
#set -e

# function to check if there is a sepi between two graphs
# output: 0 if not sepi found, 1 if sepi found, -1 if timed out
test_once () {
    OUTPUT=$(echo "
        option(merge_restriction: $merge_restriction).
        option(max_path: $max_path).
        option(timeout: $timeout).
        option(all_reductions: $all_reductions).
        option(max_reductions: $max_reductions).
        option(reduction: $reduction).
        option(stats: $stats).
        search_reduction('$filename1.bc', '$filename2.bc')." | biocham)
    last=$(echo "$OUTPUT" | tail -n 1)
    timedout=$(echo "$OUTPUT" | tail -n 3)
    statsRes=$(echo "$OUTPUT" | grep 'time:')
    echo "$statsRes"
    if [ "$last" == "no sepi found" ] # no sepi found (without even using sat solver)
    then
        resbiocham="0"
    else
        if [[ $timedout == *"timed out"* ]] # timed out
        then
            resbiocham="-1"
        else # read number of reductions, 0 if no sepi
            regex='.* (_*[0-9]*)'
            [[ $last =~ $regex ]]
            resbiocham=${BASH_REMATCH[1]}
        fi
    fi
    if [[ $OUTPUT == *"timed out"* ]]
    then 
        timed="timedout"
    else
        timed="no timedout"
    fi
}

# function to check for sepi between a pair of model
search_sepi () {
    now=$(date +"%T")
    echo "$now test between $filename1 $filename2"
    test_once # set variable $resbiocham and $timed
    echo "$filename1 $filename2 $resbiocham" >> "$output"
    echo "$resbiocham"
}

test_pair () {
    for p in $classpair ; do
        IFS='-' read -ra ADDR <<< "$p"
        set -- "${ADDR[@]}"
        filename1=$1
        filename2=$2
        search_sepi
    done
}

# variables to set manually
classname="all"
classpair=""
merge_restriction="no"
max_path="1"
timeout="1200"
all_reductions="yes"
max_reductions="200"
reduction="normal"
stats="yes"
debug="yes"
output=""
# end of variables to set manually
now=$(date +"%T")

classpair="mapk1-mapk2 mapk1-mapk3 mapk2-mapk3"
output="../test_time_mapk.txt"

echo "---"
echo "no merge normal"
merge_restriction="no"
reduction="normal"
test_pair
echo "---"

echo "---"
echo "no merge min bottom"
merge_restriction="no"
reduction="min_bottom"
test_pair
echo "---"

echo "---"
echo "no merge max bottom"
merge_restriction="no"
reduction="max_bottom"
test_pair
echo "---"

echo "---"
echo "neigh merge normal"
merge_restriction="neigh"
reduction="normal"
test_pair
echo "---"

echo "---"
echo "neigh merge min bottom"
merge_restriction="neigh"
reduction="min_bottom"
test_pair
echo "---"

echo "---"
echo "neigh merge max bottom"
merge_restriction="neigh"
reduction="max_bottom"
test_pair
echo "---"


