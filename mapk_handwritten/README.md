# Description des modèles

Expérimentations sur des graphes de réaction pour MAPK à 1,2 ou 3 niveaux, avec différents stades de réduction de tous les Michaelis-Menten 

Graphes écrits à la main ("artificiels")


MAPK1 : MM complets, MAPK2 : MM sans la réaction inverse, MAPK3 : MM réduits

Pour les cascades à 1 niveau on a étudié que celle où ne garde que le niveau K, qui est isomorphe à celle où on ne garde que KK (1_1); et celle où on ne garde que KKK est isomorphe au motif à 2 MM (réécrit dans all en 1_2): MM_SPPS.

Pour les cascades à 2 niveaux, 2_1_MAPK = celle où ne garde que les niveaux KKK et KK, 2_2_MAPK = celle où ne garde que KK et K.

Fichier all qui les rassemble tous

# Modification de scripts

Script draw_graph.py : ajout de guillemets autour des noms de modèles, pour éviter les soucis de dot avec underscore (mapk_neigh.txt et mapk_neigh.jpg)

Entretemps j'ai renommé les fichiers pour enlever les underscores

# Tests effectués :

(sous-entendu entre les versions 1 et 3, pour 3 niveaux on a aussi testé de 2 vers 3 et 1 vers 2 --> étonnamment très long : plus de 14h)

- 1 niveau, sans option/merge restriction "neigh" : plus de 200 SEPIs
- 1 niveau, maxbottom (normal/avec neigh) : 8 SEPIs (4 si on oublie symétrie du graphe d'arrivée)
- 1 niveau, minbottom : 2 SEPIs (mauvais SEPI, mais compatible avec merge_restriction)

- 1 niveau, restriction neigh et minbottom : 30 SEPIs (nb bottom = 4, bons SEPIs)

Pour 1 niveau, il faudrait aussi vérifier les motifs MM_SPPS

- 2 niveau_1/2, sans option/avec merge restriction "neigh" : plus de 200 SEPIs (avec la restriction : 2/5 min)

- 2 niveaux_1, maxbottom (normal ou avec neigh) : 16 SEPIs (nb bottom = 12)
- 2 niveaux_2, maxbottom : trop long, mais au moins un bon
- 2 niveaux_2, maxbottom, neigh : 16 SEPIs (nb bottom = 16)

- 2 niveaux_2, minbottom : 24 SEPIs
- 2 niveaux_1, minbottom, neigh : 15 (nb bottom :6)
- 2 niveaux_2, minbottom, neigh : 24 (nb nottom : 3)

- 3 niveaux, sans option  (de 1 vers 3 ou 2 ves 3) : plus de 200 SEPIs
- 3 niveaux de 1 vers 3, minbottom : plus de 200 SEPIs avec 5 bottom => mauvais SEPIs ! (mais certains respectent merge_restriction)
- 3 niveaux de 2 vers 3, minbottom : 1 (bon SEPI, avec 0 bottom)
- 3 niveaux, max_bottom (de 1 vers 3 ou 2 vers 3) : TIMEOUT

- 3 niveaux, merge restriction "neigh" : plus de 200 
- 3 niveaux, merge restriction "neigh" et minbottom : 64 
- 3 niveaux, merge restriction "neigh" et maxbottom : 165

- 3 niveaux, entre mapk1 et mapk2 avec "neigh" : l'unique SEPI est bien retrouvé, très rapidement cette fois !

- 3 niveaux, entre mapk2 et mapk3 avec "neigh" : plus de 200, minbottom 16, maxbottom 4

# Tests pour l'instant en échec :

- 3 niveaux, SEPIs de mapk1 vers mapk3 avec max_bottom : timeout supérieur à 8h
# Statistiques

(avec le script count_sepi.py)

- sans restriction :


- avec restriction neigh : 

number of tests between models: 210
number of timeouts: 0
number of pairs with no sepis: 135
number of pairs with sepis: 75
number of pairs with more than 200 sepis: 54
mean nb sepis when greater than 1 and smaller than 200: 59.95238095238095
median nb sepis when greater than 1 and smaller than 200: 39
max nb sepis: 200
min nb sepis: 1

# Estimation des temps

voir fichiers du type time_mapk_neigh.txt

Remarques :
- time_mapk :

Attention : en 3 morceaux ! Seules 208 paires ont été calculées, les 2 manquantes sont "22mapk2 22mapk1" et "22mapk3 22mapk1" (0 sepis)

- time_mapk_minbot :

Attention : en 2 morceaux ! La paire 3mapk1-3mapk3 mettait vraiment trop de temps, on sait déjà qu'il y a 200 SEPIs




