#!/bin/bash
#set -e

# function to check if there is a sepi between two graphs
# output: 0 if not sepi found, 1 if sepi found, -1 if timed out
test_once () {
    OUTPUT=$(echo "search_reduction('$filename1.bc', '$filename2.bc', merge_restriction: $merge_restriction, timeout: $timeout, all_reductions: $all_reductions, max_reductions: $max_reductions, reduction: $reduction)." | biocham)
    last=$(echo "$OUTPUT" | tail -n 1)
    timedout=$(echo "$OUTPUT" | tail -n 3)
    if [ "$last" == "no sepi found" ] # no sepi found (without even using sat solver)
    then
        resbiocham="0"
    else
        if [[ $timedout == *"timed out"* ]] # timed out
        then
            resbiocham="-1"
        else # read number of reductions, 0 if no sepi
            regex='.* (_*[0-9]*)'
            [[ $last =~ $regex ]]
            resbiocham=${BASH_REMATCH[1]}
        fi
    fi
    if [[ $OUTPUT == *"timed out"* ]]
    then 
        timed="timedout"
    else
        timed="no timedout"
    fi
}

# function to find the number of reductions once it is known that there is a sepi between two graphs
# do not use Biocham
# ouput: the number of reductions (between 1 and $sepilimit)
all_reductions () {
    SOLUTIONS=0
    cp /tmp/in.sat /tmp/tmpsat
    while : ; do
        timeout $timeout glucose -verb=0 /tmp/tmpsat /tmp/tmpout > /tmp/tmpmsg 2> /tmp/tmpmsg
        exit_status=$?
        if [[ $exit_status -eq 124 ]]; then
            break
        fi
        res="$(tail -n 1 /tmp/tmpout)"
        if [ "$res" == "UNSAT" ]; then
            break
        fi
        SOLUTIONS=$((SOLUTIONS + 1))
        if [ "$debug" == "yes" ]; then
            echo "For now $SOLUTIONS solutions."
        fi
        if [ "$SOLUTIONS" -ge "$sepilimit" ]; then
            break
        fi
        tail -n 1 /tmp/tmpout | # write the negation of the solution found
            awk '{
                for(i=1;i<NF;++i) { $i = -$i }
                print
            }' >> /tmp/tmpsat
    done
    rm -f /tmp/tmpsat
    rm -f /tmp/tmpout
    rm -f /tmp/tmpmsg
}

# function to check for sepi between a pair of model
search_sepi () {
    now=$(date +"%T")
    echo "$now test between $filename1 $filename2"
    test_once # set variable $resbiocham and $timed
    echo "$filename1 $filename2 $resbiocham" >> "$output"
    echo "$resbiocham"
}

test_all () {
    for i in $classall ; do
        for j in $classall ; do
            if [ "$i" != "$j" ]
            then
                filename1=$i
                filename2=$j
                search_sepi
            fi
        done
    done
}

test_pair () {
    for p in $classpair ; do
        IFS='-' read -ra ADDR <<< "$p"
        set -- "${ADDR[@]}"
        filename1=$1
        filename2=$2
        search_sepi
    done
}

# variables to set manually
classname="all"
classall="11mapk1 11mapk2 11mapk3 12mapk1 12mapk2 12mapk3 21mapk1 21mapk2 21mapk3 22mapk1 22mapk2 22mapk3 3mapk1 3mapk2 3mapk3"
classpair="22mapk2-21mapk2 22mapk2-21mapk3 22mapk3-11mapk1 22mapk3-11mapk2 22mapk3-11mapk3 22mapk3-12mapk1 22mapk3-12mapk2 22mapk3-12mapk3 22mapk3-21mapk1 22mapk3-21mapk2 22mapk3-21mapk3 3mapk1-11mapk1 3mapk1-11mapk2 3mapk1-11mapk3 3mapk1-12mapk1 3mapk1-12mapk2 3mapk1-12mapk3 3mapk1-21mapk1 3mapk1-21mapk2 3mapk1-21mapk3 3mapk1-22mapk1 3mapk2-11mapk1 3mapk2-11mapk2 3mapk2-11mapk3 3mapk2-12mapk1 3mapk2-12mapk2 3mapk2-12mapk3 3mapk2-21mapk1 3mapk2-21mapk2 3mapk2-21mapk3 3mapk2-22mapk1 3mapk3-11mapk1 3mapk3-11mapk2 3mapk3-11mapk3 3mapk3-12mapk1 3mapk3-12mapk2 3mapk3-12mapk3 3mapk3-21mapk1 3mapk3-21mapk2 3mapk3-21mapk3 3mapk3-22mapk1"
merge_restriction="no"
timeout="1200"
all_reductions="yes"
max_reductions="200"
reduction="min_bottom"
debug="yes"
output="mapk_minbot.txt"
# end of variables to set manually
cd $classname
now=$(date +"%T")

test_all
#test_pair

