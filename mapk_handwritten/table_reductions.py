#!/usr/bin/env python3
"""
Create a table for all reductions between two graphs
"""

import csv
import logging
import sys
from subprocess import PIPE, run

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.DEBUG)

def all_reductions(g1,g2):
    """Search all sepis between two graphs and return them in a list
    Input:
        g1 (str): file containing the first graph
        g2 (str): file containing the second graph
    Ouput:
        (list): all sepis between the graphs
    """
    logging.debug('Compute all reductions in Biocham')
    command = "option(timeout: 1200). option(all_reductions: yes). search_reduction('{}','{}'). quit.".format(g1,g2)
    result = run("biocham",
        input=command,
        stdout=PIPE, stderr=PIPE,
        universal_newlines=True)
    sepis = result.stdout.split('sepi\n')
    sepis.pop(0)
    logging.debug('All reductions returned by Biocham')
    return sepis

def read_output(sepis):
    """Transform a list of sepi in a more readable table
    Input:
        sepis (list): all sepis between the graphs
    Output:
        (list): list of sepis
    """
    logging.debug('Construct table from Biocham output')
    first = sepis.pop(0)
    operations = first.split('\n')
    title = ["sepis"]
    transf = [0]
    for operation in operations:
        try:
            title.append(operation.split(' -> ')[0])
            transf.append(operation.split(' -> ')[1])
        except IndexError:
            pass
    table = [title, transf]
    for i, sepi in enumerate(sepis):
        transf = [i+1]
        operations = sepi.split('\n')
        for operation in operations:
            try:
                transf.append(operation.split(' -> ')[1])
            except IndexError:
                pass
        table.append(transf)
    return table

def printer(table, outputFile):
    """Print table in file and find bottom minimisation
    Input:
        table (list): all sepis between the graphs
        outputFile (str): name of the output file
    """
    logging.debug('Print table to outputFile')
    minsepi = table[1]
    minimum = len(minsepi)
    for i, row in enumerate(table):
        if i != 0:
            count = 0
            for transf in row:
                if transf == "deleted":
                    count += 1
            if count < minimum:
                minimum = count
                minsepi = row
            row.append(count)
            table[i]=row
        else:
            row.append("nb bottom")
            table[i]=row
    with open(outputFile, "w") as f:
        writer = csv.writer(f)
        writer.writerows(table)
        writer.writerows([[]])
        writer.writerows([minsepi])

if __name__ == "__main__":
    logging.debug('Start program')
    g1 = sys.argv[1]
    g2 = sys.argv[2]
    outputFile = sys.argv[3]
    sepis = all_reductions(g1,g2)
    table = read_output(sepis)
    printer(table, outputFile)
