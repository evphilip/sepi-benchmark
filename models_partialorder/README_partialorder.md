# Petits modèles pour explorer les ordres partiels sur les SEPIs entre deux graphes


!!! Bug !!! En cherchant les SEPIs entre po0 et po2, Biocham envoie des espèces sur reaction0

Autre problème : le script table_reductions (avec l'option merge_restriction) ne trouve pas tous les SEPIs, notamment il ne trouve pas de SEPI où il n'y a pas de merge, mais C,E,G, reaction1 supprimés par ex. Il ne renvoie aucun SEPI où la réaction 1 est supprimée, alors qu'elle pourrait l'être.
