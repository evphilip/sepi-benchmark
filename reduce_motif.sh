#!/bin/bash

# function to reduce motifs with biocham
search_motif () {
    OUTPUT=$(echo "
        option(michaelis_menten: $michaelis_menten).
        option(r_1: $r_1).
        option(r_2: $r_2).
        option(ep: $ep).
        option(hill_reaction: $hill_reaction).
        option(partial_hill_reaction: $partial_hill_reaction).
        option(double_michaelis_menten: $double_michaelis_menten).
        pattern_reduction('${filename}_new.bc')." | biocham)
    res=$(echo "$OUTPUT" | grep 'Number of')
    echo "$res"
}

test_all () {
    for i in $classall ; do
        now=$(date +"%T")
        filename=`printf BIOMD%.10d $i`
        echo "$now reducing $filename"
        search_motif
    done
}

classall="7 8 9 10 11 21 22 26 27 28 29 30 31 34 39 43 44 45 49 55 58 73 74 78 83 89 98 109 111 115 117 122 144 145 146 166 168 169 170 171"

ca="39 43 44 45 58 98 115 117 122 145 166"
cell="7 8 109 111 144 168 169"
circ="21 22 34 55 73 74 78 83 89 170 171"
mapk="9 10 11 26 27 28 29 30 31 49 146"

classname="all"
classall=$mapk
michaelis_menten="yes"
r_1="no"
r_2="no"
ep="no"
hill_reaction="no"
partial_hill_reaction="no"
double_michaelis_menten="no"
cd $classname
now=$(date +"%T")

echo "--- r_1: $r_1 r_2: $r_2 ep: $ep"
test_all
echo "---"
