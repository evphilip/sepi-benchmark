#!/bin/bash
#set -e

# function to check if there is a sepi between two graphs
# output: 0 if not sepi found, 1 if sepi found, -1 if timed out
test_once () {
    OUTPUT=$(echo "
        option(merge_restriction: $merge_restriction).
        option(timeout: $timeout).
        option(all_reductions: $all_reductions).
        option(distinct_species: $distinct_species).
        option(max_nb_reductions: $max_reductions).
        option(extremal_sepi: $reduction).
        option(stats: $stats).
        search_reduction('$filename1.bc', '$filename2.bc')." | biocham)
    last=$(echo "$OUTPUT" | tail -n 1)
    timedout=$(echo "$OUTPUT" | tail -n 3)
    statsRes=$(echo "$OUTPUT" | grep 'time:')
    echo "$statsRes"
    if [ "$last" == "no sepi found" ] # no sepi found (without even using sat solver)
    then
        resbiocham="0"
    else
        if [[ $timedout == *"timed out"* ]] # timed out
        then
            resbiocham="-1"
        else # read number of reductions, 0 if no sepi
            regex='.* (_*[0-9]*)'
            [[ $last =~ $regex ]]
            resbiocham=${BASH_REMATCH[1]}
        fi
    fi
    if [[ $OUTPUT == *"timed out"* ]]
    then 
        timed="timedout"
    else
        timed="no timedout"
    fi
}

# function to find the number of reductions once it is known that there is a sepi between two graphs
# do not use Biocham
# ouput: the number of reductions (between 1 and $sepilimit)
all_reductions () {
    SOLUTIONS=0
    cp /tmp/in.sat /tmp/tmpsat
    while : ; do
        timeout $timeout glucose -verb=0 /tmp/tmpsat /tmp/tmpout > /tmp/tmpmsg 2> /tmp/tmpmsg
        exit_status=$?
        if [[ $exit_status -eq 124 ]]; then
            break
        fi
        res="$(tail -n 1 /tmp/tmpout)"
        if [ "$res" == "UNSAT" ]; then
            break
        fi
        SOLUTIONS=$((SOLUTIONS + 1))
        if [ "$debug" == "yes" ]; then
            echo "For now $SOLUTIONS solutions."
        fi
        if [ "$SOLUTIONS" -ge "$sepilimit" ]; then
            break
        fi
        tail -n 1 /tmp/tmpout | # write the negation of the solution found
            awk '{
                for(i=1;i<NF;++i) { $i = -$i }
                print
            }' >> /tmp/tmpsat
    done
    rm -f /tmp/tmpsat
    rm -f /tmp/tmpout
    rm -f /tmp/tmpmsg
}

# function to check for sepi between a pair of model
search_sepi () {
    now=$(date +"%T")
    echo "$now test between $filename1 $filename2"
    test_once # set variable $resbiocham and $timed
    echo "$filename1 $filename2 $resbiocham" >> "$output"
    echo "$resbiocham"
}

test_all () {
    for i in $classall ; do
        for j in $classall ; do
            if [ "$i" != "$j" ]
            then
                filename1=`printf BIOMD%.10d $j`
                filename2=`printf BIOMD%.10d $i`
                search_sepi
            fi
        done
    done
}

test_pair () {
    for p in $classpair ; do
        IFS='-' read -ra ADDR <<< "$p"
        set -- "${ADDR[@]}"
        i=$1
        j=$2
        filename1=`printf BIOMD%.10d $i`
        filename2=`printf BIOMD%.10d $j`
        search_sepi
    done
}

classall="7 8 9 10 11 21 22 26 27 28 29 30 31 34 39 43 44 45 49 55 58 73 74 78 83 89 98 109 111 115 117 122 144 145 146 166 168 169 170 171"

intra_ca="122-145 122-39 122-43 122-44 122-45 122-58 39-98 43-98 44-98 45-98 117-98 122-98 145-98 166-98 39-115 43-115 44-115 122-115 39-117 43-117 44-117 45-117 98-117 122-117 145-117 166-117 122-166"
intra_cell="109-7 144-7 109-111 144-111 109-144 144-169 7-8 109-8 144-8 169-8 7-168 109-168 144-168 109-169"
intra_circ="89-21 73-22 74-22 78-22 83-22 89-22 73-55 74-55 78-55 83-55 74-73 83-73 83-74 74-78 73-89 74-89 78-89 83-89 89-171 73-21 74-21 78-21 83-21 171-21 73-34 74-34 78-34 83-34 89-34 78-73 73-170 74-170 78-170 83-170 73-171 74-171 78-171 83-171"
intra_mapk="11-9 146-9 9-11 146-11 9-28 11-28 30-28 146-28 9-29 11-29 28-29 30-29 9-30 11-30 146-30 49-146 49-9 9-10 11-10 28-10 30-10 49-10 146-10 49-11 9-26 11-26 28-26 30-26 49-26 146-26 9-27 11-27 26-27 28-27 29-27 30-27 49-27 146-27 49-28 49-29 146-29 49-30 9-31 11-31 28-31 30-31 49-31 146-31"

inter_ca_cell="144-122 122-168 122-8 7-39 109-39 144-39 169-39 7-43 109-43 144-43 169-43 7-44 109-44 144-44 169-44 7-45 109-45 144-45 169-45 7-58 109-58 144-58 169-58 7-98 8-98 109-98 144-98 169-98 7-115 109-115 144-115 169-115 7-117 8-117 109-117 144-117 169-117 109-122 7-145 109-145 144-145 7-166 109-166 144-166 169-166"
inter_ca_circ="122-34 73-122 74-122 78-122 83-122 89-122 21-39 22-39 34-39 55-39 73-39 74-39 78-39 83-39 89-39 171-39 21-43 22-43 34-43 55-43 73-43 74-43 78-43 83-43 89-43 171-43 21-44 22-44 34-44 55-44 73-44 74-44 78-44 83-44 89-44 170-44 171-44 21-45 22-45 34-45 55-45 73-45 74-45 78-45 83-45 89-45 171-45 21-58 22-58 55-58 73-58 74-58 78-58 83-58 89-58 171-58 21-98 22-98 34-98 55-98 73-98 74-98 78-98 83-98 89-98 170-98 171-98 21-115 22-115 34-115 55-115 73-115 74-115 78-115 83-115 89-115 170-115 171-115 21-117 22-117 34-117 55-117 73-117 74-117 78-117 83-117 89-117 170-117 171-117 55-145 73-145 74-145 78-145 83-145 89-145 21-166 22-166 34-166 55-166 73-166 74-166 78-166 83-166 89-166 171-166 122-170"
inter_ca_mapk="122-29 9-122 11-122 30-122 146-122 28-145 122-10 122-27 122-31 9-39 11-39 26-39 28-39 30-39 49-39 146-39 9-43 11-43 26-43 28-43 30-43 49-43 146-43 9-44 11-44 26-44 28-44 30-44 49-44 146-44 9-45 11-45 26-45 28-45 30-45 49-45 146-45 9-58 11-58 28-58 30-58 49-58 146-58 9-98 10-98 11-98 26-98 28-98 30-98 49-98 146-98 9-115 10-115 11-115 26-115 28-115 30-115 49-115 146-115 9-117 10-117 11-117 26-117 28-117 30-117 49-117 146-117 49-122 9-145 11-145 30-145 49-145 146-145 9-166 11-166 26-166 28-166 30-166 49-166 146-166"
inter_cell_circ="74-7 83-7 144-21 144-22 144-55 109-73 144-73 109-74 109-78 144-78 109-83 144-89 73-111 74-111 78-111 83-111 83-144 73-169 74-169 78-169 83-169 89-169 144-171 22-8 55-8 73-8 74-8 78-8 83-8 89-8 109-21 109-22 109-34 144-34 109-55 55-168 73-168 74-168 78-168 83-168 89-168 109-170 144-170 109-171"
inter_cell_mapk="146-7 109-9 109-11 7-26 144-26 144-28 109-30 49-109 146-111 146-144 109-146 9-168 11-168 28-168 30-168 9-169 11-169 30-169 146-169 49-7 9-8 11-8 28-8 30-8 49-8 146-8 109-10 144-10 169-10 109-26 7-27 109-27 111-27 144-27 169-27 109-28 7-29 109-29 144-29 7-31 109-31 111-31 144-31 169-31 49-111 49-144 49-168 146-168 49-169"
inter_circ_mapk="9-21 11-21 28-21 30-21 9-22 11-22 30-22 146-22 73-26 74-26 78-26 83-26 73-28 74-28 78-28 83-28 74-30 83-30 9-34 11-34 28-34 30-34 9-55 11-55 146-55 146-73 146-74 146-78 146-83 146-89 28-170 9-171 11-171 30-171 146-171 22-10 55-10 73-10 74-10 78-10 83-10 89-10 171-10 49-21 146-21 49-22 21-27 22-27 34-27 55-27 73-27 74-27 78-27 83-27 89-27 171-27 22-29 55-29 73-29 74-29 78-29 83-29 89-29 21-31 22-31 34-31 55-31 73-31 74-31 78-31 83-31 89-31 171-31 49-34 146-34 49-55 49-73 49-74 49-78 49-83 49-89 9-170 11-170 30-170 49-170 146-170 49-171"

time_tests="122-98 43-117 144-8 7-168 21-170 83-34 29-27 11-26"
time_mapk="mapk1-mapk2 mapk1-mapk3 mapk2-mapk3"


# without timeouts
intra_ca="122-39 122-43 122-44 122-45 122-58 39-98 43-98 44-98 45-98 117-98 122-98 145-98 166-98 39-115 43-115 44-115 122-115 39-117 43-117 44-117 45-117 98-117 122-117 145-117 166-117 122-166"
intra_cell="7-8 109-8 144-8 169-8 7-168 109-168 144-168 109-169"
intra_circ="73-21 74-21 78-21 83-21 171-21 73-34 74-34 78-34 83-34 89-34 78-73 73-170 74-170 78-170 83-170 73-171 74-171 78-171 83-171"
intra_mapk="30-28 9-29 11-29 30-29 49-9 9-10 11-10 30-10 49-10 146-10 49-11 9-26 11-26 28-26 30-26 49-26 146-26 9-27 11-27 26-27 28-27 29-27 30-27 49-27 146-27 49-28 49-29 146-29 49-30 9-31 11-31 28-31 30-31 49-31 146-31"

# variables to set manually
classname="all"
classpair=""
merge_restriction="no"
max_path="1"
timeout="1200"
all_reductions="no"
distinct_species="yes"
max_reductions="200"
reduction="no"
stats="yes"
debug="yes"
output=""
# end of variables to set manually
cd $classname
now=$(date +"%T")

merge_restriction="no"
reduction="no"
stats="no"

echo "merge max bottom intra class"
all_reductions="yes"

echo "---"
echo "intra mapk"
output="../intra_specie_mapk.txt"
classpair=$intra_mapk
test_pair
echo "---"

echo "---"
echo "intra circ"
output="../intra_specie_circ.txt"
classpair=$intra_circ
test_pair
echo "---"

echo "---"
echo "intra cell"
output="../intra_specie_cell.txt"
classpair=$intra_cell
test_pair
echo "---"

echo "---"
echo "intra ca"
output="../intra_specie_ca.txt"
classpair=$intra_ca
test_pair
echo "---"
