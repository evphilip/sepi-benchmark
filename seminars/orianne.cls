% Orianne Bargain
\NeedsTeXFormat{LaTeX2e}[2017/04/15]
\ProvidesClass{orianne}[2018/01/11 Example LaTeX class]

\LoadClass{beamer}

\RequirePackage[T1]{fontenc}
\RequirePackage[english]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{stmaryrd}
\RequirePackage{amssymb}
\RequirePackage{verbatim} 		% for comments
\RequirePackage{tikz}			% for schema
\RequirePackage{graphics}
\RequirePackage{caption}		% for captions
%\RequirePackage{amsmath,amssymb}
%\RequirePackage{ulem}
%\RequirePackage{colortbl}

\RequirePackage{listings}
\RequirePackage{colortbl}
\RequirePackage{xcolor}
\definecolor{gris}{RGB}{148,154,163}
\definecolor{bleuclair}{RGB}{103, 140, 198}
\definecolor{violet}{RGB}{165,159,206}
\definecolor{vert}{RGB}{159,191,145}

% for blocks of code
\RequirePackage{fancyvrb}
\RequirePackage[linesnumbered,ruled,vlined]{algorithm2e}

% for frame number
\setbeamertemplate{footline}{%
	\hfill
	\begin{beamercolorbox}[ht=2.25ex,dp=1ex,right]{}
		\insertframenumber{}
		\hspace*{1ex}
	\end{beamercolorbox}
}

\setbeamercovered{transparent}

% for table of content
\setbeamertemplate{section in toc}{\inserttocsectionnumber.~\inserttocsection}

% for blocks
\useinnertheme[shadow=true]{rounded} % prettier blocks appearence
\usecolortheme{orchid} % Inner color theme: less agressive alternative: rose
\setbeamerfont{block title}{size={}} %normal size for block titles

\setbeamertemplate{items}[circle]
\setbeamertemplate{enumerate items}[default]

% for tikz pictures
\usetikzlibrary{arrows.meta,calc,petri,positioning,shapes,shadows,graphs}
\tikzset{%
	every place/.style={draw=violet!70,fill=violet!20,thick,
		minimum size=8mm},
	every transition/.style={draw=bleuclair!50,fill=bleuclair!20,thick,
		minimum size=6mm},
	pre/.style={<-,shorten <=1pt,>=stealth,thick},
	post/.style={->,shorten >=1pt,>=stealth,thick},
	round/.style={rounded corners=5pt},
	fire/.style={transition,fill=yellow},
	posreg/.style={->,shorten >=1pt,>=stealth,very thick,green},
	negreg/.style={-|,shorten >=1pt,>=stealth,very thick,red},
	ampersand replacement=\&,
	% https://tex.stackexchange.com/questions/55806/mindmap-tikzpicture-in-beamer-reveal-step-by-step/55849#55849
	invisible/.style={opacity=0},
	visible on/.style={alt={#1{}{invisible}}},
	alt/.code args={<#1>#2#3}{%
		\alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
	},
}