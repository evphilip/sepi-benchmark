\documentclass{orianne}


\setbeamertemplate{section in toc}{%
	{\color{black}\inserttocsectionnumber.}~\inserttocsection}
\setbeamercolor{subsection in toc}{bg=white,fg=structure}
\setbeamertemplate{subsection in toc}{%
	\hspace{1.2em}{\color{black}\rule[0.3ex]{3pt}{3pt}}~\inserttocsubsection\par}

% Couleurs Eva
\RequirePackage{xcolor}
\definecolor{bleu1}{RGB}{79, 195, 247}
\definecolor{bleu2}{RGB}{33,150,243}
\definecolor{bleu3}{RGB}{0, 0, 203}
\definecolor{violet1}{RGB}{206,147,216}
\definecolor{violet2}{RGB}{171,147,216}
\definecolor{violet3}{RGB}{142,36,170}
\definecolor{vert1}{RGB}{102, 255, 0}
\definecolor{vert2}{RGB}{76,175,80}
\definecolor{vert3}{RGB}{25, 111, 61}
\definecolor{orange1}{RGB}{255, 183, 77}
\definecolor{orange2}{RGB}{255, 152, 0}
\definecolor{orange3}{RGB}{239, 108, 0}

\begin{document}
\title{Merge Restriction \\ and \\ Graph Rewriting}   
\author{Orianne Bargain \\ Eva Philippe} 
\date{\today} 

\frame{\titlepage} 

\begin{frame}{Table of Content}
\tableofcontents[]
\end{frame}

\section{Merge Restriction}

\subsection{Motivations}	
\frame{\frametitle{Motivations}
	
\begin{figure}[!h]
	\centering
	\includegraphics[scale=0.15]{images/all_normal.jpg}
	\caption{All sepis inter and intra classes (yellow = Mapk, \\ green = Circadian Clock, blue = Cell Cycle, \\ purple = Calcium Oscillations).}
	\label{fig:all_normal}
\end{figure}
}

\frame{\frametitle{Motivations}
	
	\begin{figure}
		\begin{center}			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \node[place] (A) {A}; \& \node[transition] (r1) {$r1$}; \& \node[place] (B) {B}; \& \node[transition] (r2) {$r2$}; \& \node[place] (C) {C}; \\
			};
			
			\draw[post] (A) -- (r1);
			\draw[post] (r1) -- (B);
			\draw[post] (B) -- (r2);
			\draw[post] (r2) -- (C);
			\end{tikzpicture}
			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \& \node[place] (AC) {AC}; \& \\
				\& \node[transition] (r1) {$r1$}; \& \& \node[transition] (r2) {$r2$}; \\
				\& \& \node[place] (B) {B}; \& \\
			};
			
			\draw[post] (AC) -- (r1);
			\draw[post] (r1) -- (B);
			\draw[post] (B) -- (r2);
			\draw[post] (r2) -- (AC);
			\end{tikzpicture}
			
			\caption{Example of SEPI without biological interpretation.}
		\end{center}
	\end{figure}
	
}

\frame{\frametitle{Motivations - Merge restriction}
	
	Implementation of a merge restriction with good paths.
	
	\begin{figure}
		\begin{center}			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\node[place] (a) {a}; \& \node[transition] (u) {$u$}; \& \& \& \& \& \\
				
				\& \& \& \node[transition] (v) {$v$}; \& \node[place] (c) {c}; \& \& \\
				
				\& \& \& \& \& \& \node[place] (b) {b}; \\
			};
			
			\draw (a) -- (u);
			\draw (v) -- (c);
			\draw[dotted] (c) -- (b) node [near end, above=10pt, fill=white] {k-k'-1};
			\draw[dotted] (u) -- (v) node [near end, above=10pt, fill=white] {k'};
			\draw[dotted] (a) to[bend right] (b) node [midway, below=35pt, fill=white] {k};
			\end{tikzpicture}
			
			\caption{Example good path.}
		\end{center}
	\end{figure}
	
}

\frame{\frametitle{Motivations - Merge restriction}
	
	Explosion of the number of clauses and variables with the previous implementation ($n=|V|$).
	
	\begin{table}
		\begin{tabular}{ll}
			
			\multicolumn{2}{l}{Number of new variables:} \\
			
			$\textbf{m}_{a,b,y}$ & $\mathcal{O}(n^3)$ \\
			
			$\textbf{p}_{a,b,k}$ & $\mathcal{O}(n^3)$ \\
			
			$\textbf{p}_{a,b,k_1,c,d,k_2}$ & $\mathcal{O}(n^6)$ \\
		\end{tabular}
	\end{table}
	\begin{table}
		\begin{tabular}{ll}
			
			\multicolumn{2}{l}{Number of new clauses:} \\
			
			Inductive merge definition & $\mathcal{O}(n^3)$ \\
			
			Good path of length zero & $\mathcal{O}(n^2)$ \\
			
			Inductive good path definition & $\mathcal{O}(n^6)$ \\
			
			Good path of length k & $\mathcal{O}(n^3)$ \\
			
			Path* & $\mathcal{O}(n^3)$ \\
			
		\end{tabular}
	\end{table}
	
}

\frame{\frametitle{Motivations - Merge restriction}
	
	Previous merge restriction cannot be expressed with static local statements, thus cannot be improved easily:
	
	\begin{figure}
		\begin{center}
			\resizebox{\columnwidth}{!}{			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \node[place] (A) {A}; \& \node[transition] (r1) {$r1$}; \& \node[place] (B) {B}; \& \node[transition] (r2) {$r2$}; \& \node[place] (C) {C}; \& \node[transition] (r3) {$r3$}; \& \node[place] (D) {D}; \& \node[transition] (r4) {$r4$}; \& \node[place] (E) {E};\\
			};
			
			\draw[post] (A) -- (r1);
			\draw[post] (r1) -- (B);
			\draw[post] (B) -- (r2);
			\draw[post] (r2) -- (C);
			\draw[post] (C) -- (r3);
			\draw[post] (r3) -- (D);
			\draw[post] (D) -- (r4);
			\draw[post] (r4) -- (E);
			\end{tikzpicture}
		}
			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \node[place] (A) {A}; \& \node[transition] (r1) {$r1.r2$}; \& \node[place] (C) {C}; \& \node[transition] (r3) {$r3.r4$}; \& \node[place] (E) {E}; \\
				\& \& \node[place] (B) {B}; \& \& \node[place] (D) {D}; \\
			};
			
			\draw[post] (A) -- (r1);
			\draw[post] (r1) -- (C);
			\draw[post] (r1)  to[bend left] node[right] {} (B);
			\draw[post] (B)  to[bend left] node[right] {} (r1);
			\draw[post] (C) -- (r3);
			\draw[post] (r3) -- (E);
			\draw[post] (r3)  to[bend left] node[right] {} (D);
			\draw[post] (D)  to[bend left] node[right] {} (r3);
			\end{tikzpicture}
			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \& \& \node[place] (A) {ACE}; \& \\
				\& \& \node[transition] (r1) {$r1.r2$}; \& \& \node[transition] (r3) {$r3.r4$}; \& \\
				\& \& \node[place] (B) {B}; \& \& \node[place] (D) {D}; \\
			};
			
			\draw[post] (A) to[bend left] node[right] {} (r1);
			\draw[post] (r1) to[bend left] node[right] {} (A);
			\draw[post] (r1)  to[bend left] node[right] {} (B);
			\draw[post] (B)  to[bend left] node[right] {} (r1);
			\draw[post] (A) to[bend left] node[right] {} (r3);
			\draw[post] (r3) to[bend left] node[right] {} (A);
			\draw[post] (r3)  to[bend left] node[right] {} (D);
			\draw[post] (D)  to[bend left] node[right] {} (r3);
			\end{tikzpicture}
			\caption{Counter example for local definition.}
		\end{center}
	\end{figure}
	
}

\subsection{Definitions}

\frame{\frametitle{Definition - Graph}
	
	$G=(S,R,A)$
	
	\begin{itemize}
		\uncover<1>{\item $S$ set of species nodes.}
		\uncover<1>{\item $R$ set of reactions nodes.}
		\uncover<1>{\item $A \subseteq S \times R \cup R \times S$ set of arcs,
			
			describes how species interact through reactions.}
		\uncover<1>{\item $V = S \cup R$ set of vertices.}
		\uncover<1>{\item Sometimes arc orientation will be forgotten:
			$E \subseteq S\times R \cup R\times S$,
			$E=\{(u,v) |(u,v)\in A \vee (v,u)\in A\}$.}
	\end{itemize}
	
	\vspace{1\baselineskip}
	
	$G'=(S',R',A')$
	
}

\frame{\frametitle{Definition - Two-neighbors}
	
	\begin{block}{Definition - Two-neighbors}
		$\forall a, b \in V$
		
		\textit{a} and \textit{b} are \textit{two-neighbors} iff $\exists r \in V$ such that 
		
		\hspace{10mm} $((a,r) \in E) \wedge ((b,r) \in E)$
	\end{block}

\begin{figure}
	\begin{center}			
		\begin{tikzpicture}
		\matrix[row sep=4mm,column sep=6mm] {%
			\& \node[place] (a) {a}; \& \node[transition] (r) {$r$}; \& \node[place] (b) {b}; \\
		};
		
		\draw (a) -- (r);
		\draw (r) -- (b);
		\end{tikzpicture}
		\caption{Example two-neighbors.}
	\end{center}
\end{figure}
	
}

\frame{\frametitle{Implementation - Merge restriction}
	
	Implementation more retrictive. 
	
	Equivalent to $max path = 1$ in the previous implementation. 
	
	\begin{figure}
		\begin{center}			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \node[place] (A) {A}; \& \node[transition] (r1) {$r1$}; \& \node[place] (B) {B}; \& \node[transition] (r2) {$r2$}; \& \node[place] (C) {C}; \\
			};
			
			\draw[post] (A) -- (r1);
			\draw[post] (r1) -- (B);
			\draw[post] (B) -- (r2);
			\draw[post] (r2) -- (C);
			\end{tikzpicture}
			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \node[place] (A) {A}; \& \node[transition] (r) {$r1.r2$}; \& \node[place] (C) {C}; \\
				\& \& \node[place] (B) {B}; \& \\
			};
			
			\draw[post] (A) -- (r);
			\draw[post] (r) -- (C);
			\draw[post] (r)  to[bend left] node[right] {} (B);
			\draw[post] (B)  to[bend left] node[right] {} (r);
			\end{tikzpicture}
			
			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \node[place] (AC) {AC}; \& \node[transition] (r) {$r1.r2$}; \& \node[place] (B) {B}; \\
			};
			
			\draw[post] (r)  to[bend left] node[right] {} (AC);
			\draw[post] (AC)  to[bend left] node[right] {} (r);
			\draw[post] (r)  to[bend left] node[right] {} (B);
			\draw[post] (B)  to[bend left] node[right] {} (r);
			\end{tikzpicture}
			\caption{Example of non-allowed reduction.}
		\end{center}
	\end{figure}
	
}

\subsection{Implementation}

\frame{\frametitle{Implementation - SAT Clauses}
	
	$\forall a, b\in V$ such that a and b are not two-neighbours, $\forall y \in V'$
	
	\hspace{5mm} $	cl( \neg \textbf{m}_{a,y}	\vee \neg \textbf{m}_{b,y}) $.
	
	\vspace{1\baselineskip}
	
	Number of clauses: $ < |V|^2 \times |V'|$.
		
	Number of new variables: $0$.
	
}

\begin{frame}[fragile]\frametitle{Implementation - Data Structure}

Previous data structure:

Specie vertices: $ \llbracket 0, Number\_species - 1 \rrbracket $. 

Reaction vertices: $ \llbracket Number\_species, Number\_vertices - 1 \rrbracket $.

\begin{verbatim}
Graph = [Number_vertices, Number_species, List_edges, Id],
List_edges = [(0,4),(1,4),(4,2),(2,5),(5,0),(5,1),(2,6),
    (6,0),(6,3)].
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Implementation - Code}

\textit{a} and \textit{b} are \textit{two-neighbors} iff $\exists r \in V$ such that 

\hspace{10mm} $((a,r) \in E) \wedge ((b,r) \in E)$

\vspace{1\baselineskip}

\begin{Verbatim}[fontsize=\scriptsize]
forall(
  member(A, Vertex_G1),
  forall(
    member((A,B), Edges_G1),
    forall(
      member((B,C), Edges_G1),
      assert(neigh(A,C))
    )
  )
).
\end{Verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Implementation - Code}

$\forall a, b\in V$ such that a and b are not two-neighbours, $\forall y \in V'$

\hspace{5mm} $	cl( \neg \textbf{m}_{a,y}	\vee \neg \textbf{m}_{b,y}) $.

\vspace{1\baselineskip}

\begin{Verbatim}[fontsize=\scriptsize]
forall(
  (
    member(A, Vertex_G1),
    member(B, Vertex_G1)
  ),
  (
    (
      not(neigh(A,B))
    ->
      forall(
        member(Y, Vertex_G2),
        write(-m(a,y) -m(b,y))
      )
    ;
      true
    )
  )
).
\end{Verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Implementation - Glucose}

\begin{Verbatim}[fontsize=\small]
process_create(
  path(glucose),
  ['-verb=0',FileIn, FileOut],
  [process(Id), stderr(null), stdout(null)]
),
wait_timeout_option(Id).
\end{Verbatim}

\end{frame}

\subsection{Results}

\begin{frame}{Handwritten Mapk models}
\begin{figure}
\begin{center}
	\begin{tikzpicture}
	\node[place] (KKK) at (-4,2) {KKK};
	\node[transition] (r0) at (-3,2.5) {}; 
	\node[transition] (r1) at (-3,1.5) {}; 
	\node[place] (E1) at (-3,4) {E1};
	\node[place] (E2) at (-4,0.5) {E2};
	
	\node[place] (pKKK) at (-2,2) {pKKK};
	\node[transition] (r2) at (-2,0.5) {}; 
	\node[transition] (r3) at (0,0.5) {}; 
	\node[place] (KK) at (-3,0) {KK};
	\node[place] (pKK) at (-1,0) {pKK};
	\node[transition] (r4) at (-2,-0.5) {};
	\node[transition] (r5) at (0,-0.5) {};
	\node[place] (KKpase) at (-2,-2) {KKpase};
	
	\node[place] (ppKK) at (1,0) {ppKK};
	\node[transition] (r6) at (1,-1.5) {}; 
	\node[transition] (r8) at (1,-2.5) {}; 
	\node[place] (K) at (0,-2) {K};
	\node[place] (pK) at (2,-2) {pK};
	\node[place] (ppK) at (4,-2) {ppK};
	\node[transition] (r7) at (3,-1.5) {};
	\node[transition] (r9) at (3, -2.5) {};
	\node[place] (Kpase) at (2,-3.5) {Kpase}; 
	
	\draw[pre] (r0) -- (KKK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r0) -- (E1);
	\draw[post] (r0) -- (pKKK);
	\draw[pre] (r1) -- (pKKK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r1) -- (E2);
	\draw[post] (r1) -- (KKK);
	
	
	\draw[pre] (r2) -- (KK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r2) -- (pKKK);
	\draw[post] (r2) -- (pKK);
	\draw[pre] (r3) -- (pKK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r3) -- (pKKK);
	\draw[post] (r3) -- (ppKK);
	\draw[pre] (r4) -- (pKK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r4) -- (KKpase);
	\draw[post] (r4) -- (KK);
	\draw[pre] (r5) -- (ppKK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r5) -- (KKpase);
	\draw[post] (r5) -- (pKK);
	
	\draw[pre] (r6) -- (K);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r6) -- (ppKK);
	\draw[post] (r6) -- (pK);
	\draw[pre] (r7) -- (pK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r7) -- (ppKK);
	\draw[post] (r7) -- (ppK);
	\draw[pre] (r8) -- (pK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r8) -- (Kpase);
	\draw[post] (r8) -- (K);
	\draw[pre] (r9) -- (ppK);
	\draw[<->,shorten <=1pt,>=stealth,thick, double] (r9) -- (Kpase);
	\draw[post] (r9) -- (pK);
	\end{tikzpicture}
	\caption{Completely reduced 3-levels mapk cascade}
\end{center}
\end{figure}
\end{frame}

\begin{frame}{Handwritten Mapk models}
Cascades with 1, 2 or 3 levels, each with three reduction states of the Michaelis-Menten patterns : 15 models.
\end{frame}

\begin{frame}{Results- Mapk 3 levels}

(Timeout : 20 min, except for comparison Mapk 1 Mapk 2 without merge restriction)

\begin{table}
\centering
\resizebox{\columnwidth}{!}{
\begin{tabular}{|ll|ccc|ccc|}
\hline
& & \multicolumn{6}{c|}{Nb SEPIs} \\
\hline
& & \multicolumn{3}{c|}{Without merge restriction} & \multicolumn{3}{c|}{With merge restriction} \\
\multicolumn{2}{|c|}{Comparison} & Normal & Min $\bot$ & Max $\bot$ & Normal & Min $\bot$ & Max $\bot$ \\
\hline
\hline
Mapk 1 & Mapk 2 & 1 & 1 & 1 & 1 & 1 & 1 \\
\hline
Mapk 1 & Mapk 3 & $200^+$ & \cellcolor{red!50} $200^+$ & timeout & $200^+$ & \cellcolor{green!50} 165 & \cellcolor{green!50} 64 \\
\hline
Mapk 2 & Mapk 3 &  $200^+$ &\cellcolor{green!50} 1 & timeout & $200^+$ & \cellcolor{red!20} 16 & \cellcolor{green!50} 4 \\
\hline
\end{tabular}
}
\caption{Number of Sepi relations for 3-levels mapk models (Mapk 1 = complete, Mapk 2 = no reverse reactions, Mapk 3 = reduced).} \label{tab:eval_mapk_rewriten}
\end{table}
Colour red when all the SEPIs are bad.

Light red : between Mapk 1 and Mapk 2 we expect a SEPI with 0 deletion, but it is not allowed with the strong merge restriction.
\end{frame}

\begin{frame}{Results - All handwritten Mapk}
(210 pairs)

\begin{table}	
\resizebox{\columnwidth}{!}{
\begin{tabular}{|l|cccc|c|c|}
\hline
& \multicolumn{4}{c|}{Pairs with SEPIs} & No SEPI & Timeouts \\
Reduction & $200^+$ & $<200$ & mean & median & & (20 min) \\
\hline
\hline
Normal & 64 (30.5\%) & 9 (4\%) & 25 & 14 & 127 (60.5\%) & 10 (5\%) \\
Merge & 54 (26\%) & 21 (10\%) & 60 & 39 & 135 (64\%) & 0 \\
\hline
Normal Min $\bot$ & 20 (9.5\%) & 54 (25.7\%) & 24 & 5.5 & 127 (60.5\%) & 9 (4.3\%) \\
Merge Min $\bot$ & 10 (5\%) & 64 (30.5\%) & 37 & 16 & 135 (64\%) & 1 (0.5\%) \\
\hline
Normal Max $\bot$ & 0 & 48 (23\%) & 18.7 & 4 & 127 (60.5\%) & 35 (16.5\%) \\ 
Merge Max $\bot$ & 0 & 74 (35\%) & 15.6 & 4 & 135 (64\%) & 1 (0.5\%) \\
\hline
\end{tabular}
}
\end{table}

Mean and median are computed on the set of pairs that have between 1 and 200 SEPIs.
\end{frame}



\begin{frame}{Combinations of 2 Michaelis-Menten patterns}

Up to symmetries, 23 possibilities to merge 2 Michaelis-Menten patterns (reduced forms), denoted pattern $a$ and pattern $b$
\vfill
3 classes according to the number of merged species.
\vfill
These small models already present much more SEPIs than expected.

Example : merging of two species "SP-PS"

\begin{center}
	
	\begin{tikzpicture}
	\node[place] (aE) at (0,0) {aE};    
	\node[place] (aSbP) at (-2,-2) {aSbP};
	\node[place] (aPbS) at (2,-2) {aPbS};
	\node[place] (bE) at (0,-4) {bE};
	\node[transition] (r0) at (0,-1) {$aR$}; 
	\node[transition] (r1) at (0,-3) {$bR$};
	
	
	\draw[pre] (r0) -- (aSbP);
	\draw[pre] (r0) to[bend left] (aE);
	\draw[post] (r0) to[bend right] (aE);
	\draw[post] (r0) -- (aPbS);
	
	\draw[pre] (r1) -- (aPbS);
	\draw[pre] (r1) to[bend left] (bE);
	\draw[post] (r1) to[bend right] (bE);
	\draw[post] (r1) -- (aSbP);
	\end{tikzpicture}
	
\end{center}

\end{frame}

\begin{frame}{Results - Combinations of 2 Michaelis-Menten}

(2070 pairs, no timeout superior to 20 min)

\begin{table}
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{|l|cccc|c|}
			\hline
			& \multicolumn{4}{c|}{Pairs with SEPIs} & No SEPI \\
			Restriction & $200^+$ & $<200$ & mean & median &  \\
			\hline
			\multicolumn{1}{c}{Complete and intermediary} \\
			\hline
			Normal & 0 & 158 (7.6\%) & 1.65 & 2 & 1912 (92.4\%) \\
			Merge & 0 & 23 (1\%) & 1.6 & 2 & 2047 (99\%) \\
			\hline
			\multicolumn{1}{c}{Intermediary and Reduced} \\
			\hline
			Normal & 7 (0.3\%) & 225 (10.9\%)& 17.5 & 4 & 1838 (88.8\%)\\
			Merge & 0 & 76 (3.7\%) & 10.3 & 6 & 1994 (96.3\%) \\
			\hline
			\multicolumn{1}{c}{Complete and Reduced} \\
			\hline
			Normal & 92 (4.4\%) & 260 (12.6\%) & 25 & 4 & 1718 (83\%)\\
			Merge & 19 (0.9\%)& 173 (8.4\%)& 29 & 18 & 1878 (90.7\%)\\
			\hline
			\multicolumn{1}{c}{Total} \\
			\hline
			Normal & 99 (1.6\%) & 643 (10.3\%) & & & 5468 (88\%) \\
			Merge & 19 (0.3\%) & 272 (4.4\%) & & & 5919 (95.3\%)\\
			\hline
		\end{tabular}
	}
	
\end{table}
\end{frame}

% Orianne

\frame{\frametitle{Results - BioModels Inter class}
	
	All sepis inter classes (yellow = Mapk, green = Circadian Clock, blue = Cell Cycle, purple = Calcium Oscillations).

	\begin{figure}[!h]
		\resizebox{\columnwidth}{!}{
			\includegraphics[scale=0.15]{images/inter_normal_all.jpg}
		}
		\caption{Without merge restriction.}
		\label{fig:inter_normal}
	\end{figure}
	
	\begin{figure}[!h]
		\resizebox{\columnwidth}{!}{
		\includegraphics[scale=0.15]{images/inter_merge_neigh_all.jpg}
		}
		\caption{With merge restriction.}
		\label{fig:inter_merge}
	\end{figure}

}
	
\frame{\frametitle{Results - Biomodels Intra class}
	
	All sepis intra classes (yellow = Mapk, green = Circadian Clock, blue = Cell Cycle, purple = Calcium Oscillations).
	
	\begin{figure}[!h]
		\resizebox{\columnwidth}{!}{
		\includegraphics[scale=0.15]{images/intra_normal_all.jpg}
		}
		\caption{Without merge restriction.}
		\label{fig:intra_normal}
	\end{figure}
	
	\begin{figure}[!h]
		\resizebox{\columnwidth}{!}{
		\includegraphics[scale=0.15]{images/intra_merge_neigh_all.jpg}
		}
		\caption{With merge restriction.}
		\label{fig:intra_merge}
	\end{figure}
}

\frame{\frametitle{Results - Intra class - New models - timeout 20 min}
	
\begin{table}
		
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{|l|l|ccc|c|c|}
			\hline
			 & & \multicolumn{3}{c|}{SEPI} & No SEPI & Timeout \\
			Class & Reduction & $200^+$ & $<200$ & total & & \\
			\hline
			\hline
			Ca (110) & Normal & 20 & 6 & 26 (23.64\%) & 83 (75.45\%) & 1 (00.91\%) \\
			& Merge & 11 & 8 & 19 (17.27\%) & 91 (82.73\%) & 0 (00.00\%) \\
			\hline
			Cell (42) & Normal & 8 & 0 & 8 (19.05\%) & 28 (66.67\%) & 6 (14.28\%) \\
			& Merge & 5 & 0 & 5 (11.90\%) & 34 (80.95\%) & 3 (07.15\%) \\
			\hline
			Circ (110) & Normal & 16 & 3 & 19 (17.27\%) & 70 (63.64\%) & 21 (19.09\%) \\
			& Merge & 10 & 0 & 10 (09.09\%) & 100 (90.91\%) & 0 (00.00\%)\\
			\hline
			Mapk (110) & Normal & 35 & 0 & 35 (31,82\%) & 62 (56,36\%) & 13 (11,82\%) \\
			& Merge & 13 & 4 & 17 (15.45\%) & 87 (79.09\%) & 6 (05.46\%) \\
			\hline
			\hline
			Total (372) & Normal & 79 & 9 & 88 (\textcolor{red}{23,66\%}) & 243 (65,32\%) & 41 (\textcolor{green}{11,02\%}) \\
			& Merge & 39 & 12 & 51 (\textcolor{red}{13.71\%}) & 312 (83.87\%) & 9 (\textcolor{green}{02.42\%}) \\
			\hline
		\end{tabular}
	}
	\caption{SEPIs intra class (time in ms).}
	
\end{table}
}

\frame{\frametitle{Results - Inter class - New models - timeout 20 min}
	
\begin{table}
	\centering
	\resizebox{\columnwidth}{!}{
	\begin{tabular}{|lll|l|ccc|}
		\hline
		\multicolumn{3}{|c|}{Results inter class} & Reduction & Sepis & No Sepis & Timeouts \\
		\hline
		\hline
		Ca Oscillations & Cell Cycle & (154) & Normal & 43 (27.92\%) & 109 (70.78\%) & 2 (01.30\%) \\
		 & & & Merge & 33 (21.43\%) & 121 (78.57\%) & 0 (00.00\%) \\
		\hline
		Ca Oscillations & Circ Clock & (242) & Normal & 100 (41.32\%) & 136 (56.20\%) & 6 (02.48\%) \\
		 & & & Merge & 4 (01.65\%) & 238 (98.65\%) & 0 (00.00\%) \\
		\hline
		Ca Oscillations & Mapk & (242) & Normal & 74 (30.58\%) & 162 (66.94\%) & 6 (02.48\%) \\
		 & & & Merge & 51 (21.08\%) & 190 (78.51\%) & 1 (0.41\%) \\
		\hline
		Cell Cycle & Circ Clock & (154) & Normal & 22 (14.29\%) & 109 (70.78\%) & 23 (14.93\%) \\
		 & & & Merge & 7 (04.55\%) & 142 (92.21\%) & 5 (03.24\%) \\
		\hline
		Cell Cycle & Mapk & (154) & Normal & 30 (19.48\%) & 105 (68.18\%) & 19 (12.34\%) \\
		 & & & Merge & 9 (05.84\%) & 135 (87.66\%) & 10 (06.50\%) \\
		\hline
		Circ Clock & Mapk & (242) & Normal & 52 (21.49\%) & 155 (64.05\%) & 35 (14.46\%) \\
		 & & & Merge & 10 (04.13\%) & 230 (95.04\%) & 2 (00.83\%) \\
		\hline
		\hline
		\multicolumn{2}{|c}{Total} & (1188) & Normal & 321 (\textcolor{green}{27.02\%}) & 776 (65.32\%) & 91 (\textcolor{green}{7.66\%}) \\
		 & & & Merge & 114 (\textcolor{green}{09.60\%}) & 1056 (88.89\%) & 18 (\textcolor{green}{01.51\%}) \\
		\hline
	\end{tabular}
	}
\caption{SEPIs inter class (time in ms).}
\end{table}
}

\frame{\frametitle{Results - Execution time}
	
	\begin{table}
		\resizebox{\columnwidth}{!}{
			\begin{tabular}{|l||r|rrrr|rrrr|rrrr||r|rrrr|rrrr|rrrr|}
				\hline
				Model & \multicolumn{13}{c||}{No Restriction} & \multicolumn{13}{c|}{Merge Restriction} \\
				\hline
				& & \multicolumn{4}{c|}{Normal} & \multicolumn{4}{c|}{Min $\bot$} & \multicolumn{4}{c||}{Max $\bot$} & & \multicolumn{4}{c|}{Normal} & \multicolumn{4}{c|}{Min $\bot$} & \multicolumn{4}{c|}{Max $\bot$} \\
				& time & nb & 1st & 2nd & last & nb & 1st & 2nd & last & nb & 1st & 2nd & last & time & nb & 1st & 2nd & last & nb & 1st & 2nd & last & nb & 1st & 2nd & last \\
				\hline
				\hline
				122-098 (ca) & 26 & 200 & 10 & 11 & 1406 & 200 & 4 & 4 & 7 & 200 & 109747 & 98568 & 146762 & 792 & 200 & 25 & 19 & 1881 & 160 & 1448 & 1377 & 2986 & 200 & 623 & 632 & 933 \\
				043-117 (ca) & 5 & 140 & 4 & 4 & 47 & 28 & 2 & 1 & 1 & 16 & 2 & 2 & 3 & 10 & 32 & 5 & 4 & 7 & 16 & 2 & 1 & 3 & 16 & 2 & 1 & 3 \\
				144-008 (cell) & 591 & 200 & 97 & 93 & 316 & 200 & 63 & 106 & 289 & - & t & - & - & 11512 & 200 &  836 & 1101 & 1033 & 200 & 186 & 203 & 420 & 30 & 198 & 210 & 271 \\
				007-168 (cell) & 687 & 200 & 200 & 159 & 360 & - & t & - & - & - & t & - & - & 5950 & 0 & (570) & - & - & 0 & (58) & - & - & 0 & (56) & - & - \\
				021-170 (circ) & 164 & 200 & 7316 & 1282 & 7775 & 200 & 1186 & 536 & 396 & 200 & 2017 & 1411 & 2216 & 428 & 0 & (76) & - & - & 0 & (9) & - & - & 0 & (9) & - & - \\
				083-034 (circ) & 873 & 200 & 333 & 185 & 531 & 200 & 62852 & 61760 & 63432 & - & t & - & - & 7393 & 200 &  528 & 488 & 835 & 200 & 832 & 806 & 1528 & 200 & 805 & 651 & 731 \\
				029-027 (mapk) & 26 & 72 & 12 & 12 & 29 & 1 & 3 & (2) & - & 4 & 3 & 3 & 3 & 21 & 72 & 12 & 12 & 30 & 1 & 3 & (3) & - & 4 & 3 & 3 & 3 \\
				011-026 (mapk) & 485 & 200 & 45258 & 45204 & 48402 & 200 & 75755 & 74830 & 85844 & - & t & - & - & 1682 & 0 & (469) & - & - & 0 & (46) & - & - & 0 & (50) & - & - \\
				\hline
				mapk1-mapk2 & 3193 & 1 & t & - & - & 1 & t & - & - & 1 & t & - & - & 4340 & 1 & 1677 & (3679) & - & 1 & 426 & (374) & - & 1 & 411 & (373) & - \\
				mapk1-mapk3 & 796 & 200 & t & - & - & 200 & 144562 & 146264 & 150344 & - & t & - & - & 4223 & 200 & 607 & 603 & 3427 & 165 & 459 & 539 & 835 & 64 & 456 & 456 & 504 \\
				mapk2-mapk3 & 460 & 200 & t & - & - & 1 & 988840 & (988642) & - & - & t & - & - & 1574 & 200 & 234 & 216 & 2729 & 16 & 77 & 119 & 120& 4 & 94 & 98 & 98 \\
				\hline
			\end{tabular}
		}
		\caption{Time to compute the first second and last SEPI for each problem (time in ms).} \label{tab:eval_time}
	\end{table}
	
}

\subsection{What's new in Biocham?}
\begin{frame}[fragile]\frametitle{What's new in Biocham?}

New option type:

\begin{verbatim}
reduction
  normal 
  min_bottom
  max_bottom

merge_restriction
  no
  accurate
  neigh
  old
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{What's new in Biocham?}

Example:

\begin{verbatim}
option(mapping_restriction: []).
option(merge_restriction: accurate).
option(max_path: 3).
option(timeout: 180).
option(all_reductions: yes).
option(max_reductions: 200).
option(reduction: min_bottom).
option(stats: yes).

search_reduction(
  library/examples/sepi/MM1.bc, 
  library/examples/sepi/MM2.bc).
\end{verbatim}

\end{frame}

\section{Pattern Reduction}

\subsection{Motivations}

\begin{frame}{Motivations - Pattern reduction}

\begin{center}
	
	\begin{tikzpicture}
	\node[place, draw=red] (E) at (0,3) {E};         
	\node[place, fill=bleu3] (aS) at (-4,2) {aS};
	\node[place, fill=violet3] (aC) at (-2,2) {aC};
	\node[place, fill=vert3] (aPbS) at (0,2) {aPbS};
	\node[place, fill=violet1] (bC) at (2,2) {bC}; 
	\node[place, fill=bleu3] (bP) at (4,2) {bP};
	\node[transition, fill=bleu1] (r0) at (-3, 2) {$R_0$}; 
	\node[transition, fill=vert1] (r1) at (-1, 2) {$R_1$};
	\node[transition, fill=vert1] (r2) at (1, 2) {$R_{2}$};
	\node[transition, fill=bleu1] (r3) at (3, 2) {$R_3$}; 
	
	
	\draw[pre] (r0) -- (aS);
	\draw[pre] (r0) |- (E);
	\draw[post] (r0) -- (aC);
	
	
	\draw[pre] (r1) -- (aC);
	\draw[post] (r1) -- (E);
	\draw[post] (r1) -- (aPbS);
	
	\draw[pre] (r2) -- (aPbS);
	\draw[pre] (r2) -- (E);
	\draw[post] (r2) -- (bC);
	
	\draw[pre] (r3) -- (bC);
	\draw[post] (r3) |- (E);
	\draw[post] (r3) -- (bP);
	
	\end{tikzpicture}
	
	Combination of two Michaelis-Menten, with shared enzyme and first product=second substrate (EE-PS)
	
	\begin{tikzpicture}
	\node[place, fill=bleu3] (aE) at (0,0) {E};    
	\node[place, fill=violet1] (aSbP) at (-2,-2) {aSbP};
	\node[place, fill=violet3] (aPbS) at (2,-2) {aPbS};
	\node[place, fill=vert3] (bE) at (0,-4) {bE};
	\node[transition, fill=bleu1] (r0) at (0,-1) {$R_0$}; 
	\node[transition, fill=vert1] (r1) at (0,-3) {$R_1$};
	
	
	\draw[pre] (r0) -- (aSbP);
	\draw[pre] (r0) to[bend left] (aE);
	\draw[post] (r0) to[bend right] (aE);
	\draw[post] (r0) -- (aPbS);
	
	\draw[pre] (r1) -- (aPbS);
	\draw[pre] (r1) to[bend left] (bE);
	\draw[post] (r1) to[bend right] (bE);
	\draw[post] (r1) -- (aSbP);
	
	\end{tikzpicture}
	Another combination of two Michaelis-Menten (SP-PS)
\end{center}

\end{frame}


\begin{frame}{Towards SISOc}
%
%SISO problem (Subgraph Isomorphism) is not sufficient because we would also like to constrain the complex ES not to interfere in other reactions and the reactions $R_1$, $R_{-1}$ and $R_2$ not to use other species. These constraints also allow to have the commutativity property of patterns reductions.

\begin{figure}
\begin{center}

\begin{tikzpicture}
\matrix[row sep=3mm,column sep=4mm] {%
\& \& \node[place] (E) {E}; \& \& \&\\
\& \& \node[transition,draw=red] (r1) {$R_1$}; \& \& \&\\
\& \node[place] (S) {S}; \& \& \node[place, draw=red] (ES) {ES};
\& \node[transition,draw=red] (r3) {$R_2$}; \& \node[place] (P) {P};\\
\& \& \node[transition, draw=red] (r2) {$R_{-1}$}; \& \& \&\\
};

\draw[pre] (r1) -- (E);
\draw[pre] (r1) -- (S);
\draw[post] (r1) -- (ES);
\draw[pre] (r2) -- (ES);
\draw[post] (r2) -- (S);
\draw[pre] (r3) -- (ES);
\draw[post] (r3) -- (P);
\draw[round, post] (r2) -- ++(-23mm,0) |- (E);
\draw[round, post] (r3) -- (r3 |- E) -- (E);

\end{tikzpicture}
\caption{Complete Michaelis-Menten reaction graph}

			\begin{tikzpicture}
			\matrix[row sep=4mm,column sep=6mm] {%
				\& \& \node[place] (E) {E}; \& \\
				\& \node[place] (S) {S}; \& \node[transition] (r) {$R$}; \& \node[place] (P) {P}; \\
			};
			
			\draw[pre] (r) -- (S);
			\draw[post] (r) -- (P);
			\draw[post] (r)  to[bend left] node[right] {} (E);
			\draw[post] (E)  to[bend left] node[right] {} (r);
			\end{tikzpicture}
			\caption{Reduced Michaelis-Menten reaction graph}

\end{center}
\end{figure}

\end{frame}

\begin{frame}{Another pattern : distributive MM}

From Markevich, 2004, \emph{Signaling switches and bistability ...}

Model for dephosphorylation : $E+S \Leftrightarrow ES \rightarrow EP \Leftrightarrow E+P$

\begin{center}
	
	\begin{tikzpicture}
	\node[place] (E) at (0,4) {E};    
	\node[place] (ES) at (-1,0) {ES};
	\node[place] (S) at (-4.5,0) {S};
	\node[place] (EP) at (1,0) {EP};
	\node[place] (P) at (4.5,0) {P};
	\node[transition] (r0) at (-3,2) {$R_1$}; 
	\node[transition] (r1) at (-2,-2) {$R_{-1}$}; 
	\node[transition] (r2) at (0,0) {$R_3$}; 
	\node[transition] (r3) at (3,2) {$R_2$}; 
	\node[transition] (r4) at (2,-2) {$R_{-2}$}; 
	
	
	\draw[pre] (r0) to[bend left] (E);
	\draw[pre] (r0) -- (S);
	\draw[post] (r0) -- (ES);
	\draw[pre] (r1) -- (ES);
	\draw[post] (r1) -- (S);
	\draw[post] (r1) to[bend left] (E);
	\draw[pre] (r2) -- (ES);
	\draw[post] (r2) --(EP);
	\draw[pre] (r3) -- (EP);
	\draw[post] (r3) to[bend right] (E);
	\draw[post] (r3) -- (P);
	\draw[post] (r4) -- (EP);
	\draw[pre] (r4) to[bend right] (E);
	\draw[pre] (r4) -- (P);
	\end{tikzpicture}
	
\end{center}

\end{frame}

% inclure les autres patterns (ESS etc, si on a eu le temps de les tester)

%\begin{frame}{Reductions of variants of Michaelis-Menten}
%\begin{table}
%	\centering
%	\resizebox{\columnwidth}{!}{
%		\begin{tabular}{|ll|ccc|ccc|}
%			\hline
%			& & \multicolumn{6}{c|}{Nb SEPIs} \\
%			\hline
%			& & \multicolumn{3}{c|}{Without merge restriction} & \multicolumn{3}{c|}{With merge restriction} \\
%			\multicolumn{2}{|c|}{Comparison} & Normal & Min $\bot$ & Max $\bot$ & Normal & Min $\bot$ & Max $\bot$ \\
%			\hline
%			\hline
%			SES & MM & $200^+$ &  1 & 20 & $200^+$ &  \cellcolor{red!20}{6} & 20 \\
%			\hline
%			ESS & MM & 85  & 1 & 10 & 66  & 1 & 10\\
%			\hline
%			SEE & MM &  179  & 1 & 8 & 141  & \cellcolor{red!20} 10 & 8\\
%			\hline
%			ESP & MM & $200^+$ &\cellcolor{red!50} 2 & 12 & 173 & \cellcolor{red!50} 2 & 12\\
%			\hline
%		\end{tabular}
%	}
%	\caption{Number of SEPIs without and with strong merge restriction}
%\end{table}
%
%Red : All SEPIs given are "bad" (not among the expected ones)
%
%Light red : the SEPIs are not bad, but they are bigger than expected
%
%
%\end{frame}

% Orianne

\subsection{Implementation}

\begin{frame}[fragile]\frametitle{Implementation - Data Structure}

Previous data structure:

\vspace{1\baselineskip}

Specie vertices: $ \llbracket 0, Number\_species - 1 \rrbracket $. 

Reaction vertices: $ \llbracket Number\_species, Number\_vertices - 1 \rrbracket $.

\begin{verbatim}
Graph = [Number_vertex, Number_species, List_edges, Id]
List_edges = [(0,4),(1,4),(4,2),(2,5),(5,0),(5,1),(2,6),
    (6,0),(6,3)]
\end{verbatim}

New data structure:

\begin{verbatim}
Graph = [Number_vertex, Number_species, List_edges, Id]
Normal arcs: arcs{0:[4],1:[4],2:[5,6],4:[2],5:[0,1],
    6:[0,3]} 
Inversed arcs: arcs{0:[5,6],1:[5],2:[4],3:[6],4:[0,1],
    5:[2],6:[2]}
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Implementation - Code}

\begin{verbatim}
michaelis_menten_pattern(+ES, -E, -S, -P):-
  Ingoing_ES = [R1],
  Outgoing_ES = [R_1, R2],
  Ingoing_R1 = [E, S],
  Outgoing_R1 = [ES],
  Ingoing_R_1 = [ES],
  Outgoing_R_1 = [E, S],
  Ingoing_R2 = [ES],
  Outgoing_R2 = [E, P].
\end{verbatim}

\end{frame}

\begin{frame}[fragile]\frametitle{Implementation - Code}

\begin{verbatim}
forall(
  member(ES, Species_list),
  (
    (
      michaelis_menten_pattern(+ES, -E, -S, -P))
    ->
      write("E+S=>E+P.")
    ;
      true
    )
  )
).
\end{verbatim}

\end{frame}

\subsection{Results}

\frame{\frametitle{Results - Models with Michaelis Menten Pattern}
	
	
	\begin{table}
		\resizebox{\columnwidth}{!}{
		\begin{tabular}{|lc|r||c|c|c|}
			\hline
			Class & & Model & With R-1 & Without R-1 & With R-1, R-2, EP \\
			\hline
			\hline
			Ca & (11) & 39 & 0 & 1 & 0 \\
			\hline
			Cell & (7) & 109 & 8 & 0 & 0 \\
			& & 169 & 0 & 1 & 0 \\
			\hline
			Circ & (11) & - & - & - & - \\
			\hline
			Mapk & (11) & 9 & 10 & 0 & 0 \\
			& & 11 & 10 & 0 & 0 \\
			& & 26 & 2 & 0 & 2 \\
			& & 28 & 4 & 0 & 3 \\
			& & 30 & 4 & 0 & 4 \\
			& & 49 & 13 & 0 & 0 \\
			\hline
		\end{tabular}
		}
		\caption{Number of Michaelis-Menten patterns.}
	\end{table}

Mapk models without any pattern: 10, 27, 29, 31, 146.
	
}

\frame{\frametitle{Results - Pattern Reduction - Mapk - Timeout 20 min}
	
\begin{table}
	\resizebox{\columnwidth}{!}{
		\begin{tabular}{|l|ccc|c|c|}
			\hline
			& \multicolumn{3}{c|}{SEPI} & No SEPI & Timeout \\
			& $200^+$ & $<200$ & total & & \\
			\hline
			Normal & 35 & 0 & 35 (31,82\%) & 62 (56,36\%) & 13 (11,82\%) \\
			Reduced & 22 & 6 & 28 (25.45\%) & 81 (73.64\%) & 1 (0.91\%) \\
			\hline
		\end{tabular}
	}
	\caption{SEPIs in Mapk class (110 pairs, time in ms).}
\end{table}
}

\frame{\frametitle{Results - Pattern Reduction - Mapk - Timeout 20 min}
	
	\begin{columns}
		\begin{column}{0.65\textwidth}
			\begin{figure}[!h]
				\resizebox{\columnwidth}{!}{
					\includegraphics[scale=0.15]{images/intra_normal_mapk.jpg}
				}
				\caption{SEPIs on Mapk without pattern reduction.}
				\label{fig:intra_mapk}
			\end{figure}
		\end{column}
		\begin{column}{0.35\textwidth}  %%<--- here
			\begin{figure}[!h]
				\resizebox{\columnwidth}{!}{
					\includegraphics[scale=0.15]{images/intra_mapk_pattern.jpg}
				}
				\caption{SEPIs on Mapk with pattern reduction.}
				\label{fig:intra_mapk_reduced}
			\end{figure}
		\end{column}
	\end{columns}
	
	
	
	

}

\begin{frame}{Other patterns}

Hill  : 2 distinct ligant sites

\begin{center}
	
	\begin{tikzpicture}
	\node[place] (E) at (-4,2) {E};    
	\node[place] (ES) at (0,2) {ES};
	\node[place] (S) at (-2,0) {S};
	\node[place] (SE) at (-4,-2) {SE};
	\node[place] (SES) at (0,-2) {SES};
	\node[place] (P) at (4,-2) {P};
	\node[transition] (r0) at (-2,2) {$R_{1_1}$}; 
	\node[transition] (r1) at (0,0) {$R_{1_2}$}; 
	\node[transition] (r2) at (-4,0) {$R_{2_1}$}; 
	\node[transition] (r3) at (-2,-2) {$R_{2_2}$}; 
	\node[transition] (r4) at (2,-2) {$R_3$};
	
	
	\draw[pre] (r0) -- (E);
	\draw[pre] (r0) -- (S);
	\draw[post] (r0) -- (ES);
	\draw[pre] (r1) -- (ES);
	\draw[pre] (r1) -- (S);
	\draw[post] (r1) -- (SES);
	\draw[pre] (r2) -- (E);
	\draw[pre] (r2) -- (S);
	\draw[post] (r2) -- (SE);
	\draw[pre] (r3) -- (SE);
	\draw[pre] (r3) -- (S);
	\draw[post] (r3) -- (SES);
	\draw[pre] (r4) -- (SES);
	\draw[post] (r4) to[bend right] (E);
	\draw[post] (r4) -- (P);
	
	\end{tikzpicture}
	
\end{center}

\end{frame}

\begin{frame}{Other patterns}

Partial Hill :  : twice the same ligant site

\begin{center}

\begin{tikzpicture}
\node[place] (E) at (-4,2) {E};    
\node[place] (ES) at (-2,0) {ES};
\node[place] (S) at (-4,-2) {S};
\node[place] (ESS) at (1.5,0) {ESS};
\node[place] (P) at (4.5,0) {P};
\node[transition] (r0) at (-4,0) {$R_1$}; 
\node[transition] (r1) at (0,-1) {$R_2$}; 
\node[transition] (r2) at (3,1) {$R_3$}; 


\draw[pre] (r0) -- (E);
\draw[pre] (r0) -- (S);
\draw[post] (r0) -- (ES);
\draw[pre] (r1) -- (ES);
\draw[pre] (r1) -- (S);
\draw[post] (r1) -- (ESS);
\draw[pre] (r2) -- (ESS);
\draw[post] (r2) to[bend right] (E);
\draw[post] (r2) -- (P);

\end{tikzpicture}

\end{center}

\end{frame}

\begin{frame}{Other patterns}

Double Michaelis-Menten : 2 forms of the enzyme

\begin{center}

\begin{tikzpicture}
\node[place] (E) at (-4,2) {E};    
\node[place] (ES) at (-2,0) {ES};
\node[place] (S) at (-4,-2) {S};
\node[place] (ESS) at (1,-2) {ESS};
\node[place] (P) at (4.5,0) {P};
\node[transition] (r0) at (-4,0) {$R_{1_1}$}; 
\node[transition] (r1) at (-1,-2) {$R_{2_1}$}; 
\node[transition] (r2) at (3,-2) {$R_{2_2}$}; 
\node[transition] (r3) at (1,1) {$R_{1_2}$}; 


\draw[pre] (r0) -- (E);
\draw[pre] (r0) -- (S);
\draw[post] (r0) -- (ES);
\draw[pre] (r1) -- (ES);
\draw[pre] (r1) -- (S);
\draw[post] (r1) -- (ESS);
\draw[pre] (r2) -- (ESS);
\draw[post] (r2) to[bend right] (ES);
\draw[post] (r2) -- (P);
\draw[pre] (r3) -- (ES);
\draw[post] (r3) to[bend right] (E);
\draw[post] (r3) -- (P);
\end{tikzpicture}

\end{center}

\end{frame}


\frame{\frametitle{Results - Models with Patterns}
	
	
	\begin{table}
			\begin{tabular}{|lc|c|c|c|}
				\hline
				Class & & Hill & Partial Hill & Double MM \\
				\hline
				\hline
				Ca & (11) & 0 & 0 & 0 \\
				\hline
				Cell & (7)  & 0 & 0 & 0 \\
				\hline
				Circ & (11)  & 0 & 0 & 0 \\
				\hline
				Mapk & (11)  & 0 & 0 & 0 \\
				\hline
			\end{tabular}
		\caption{Number of patterns.}
	\end{table}
	
}

\subsection{What's new in Biocham?}

\begin{frame}[fragile]\frametitle{What's new in Biocham? - Pattern}

Example:

\begin{verbatim}
option(michaelis_menten: yes).
option(r_1: yes).
option(r_2: no).
option(ep: no).
option(enzyme: yes).

pattern_reduction("library/examples/sepi/MM1.bc").
\end{verbatim}

Output:

\begin{verbatim}
Number of Michaelis Menten patterns: 1
\end{verbatim}

Create a new file: library/examples/sepi/MM1\_reduced.bc.

\end{frame}

%Eva

\section{Problems with inhibitors}

\begin{frame}{Tackling inhibitors}
\begin{definition}
A reaction inhibitor is a substance that decreases the rate of, or prevents, a chemical reaction.
\end{definition}

\bigskip

With the curation in Biocham, a species $S$ is inferred as an inhibitor for reaction $R$ whenever Biocham does not manage to determine if its rate varies always positively or negatively with the concentration of $S$.

\bigskip 

With the graph representation of chemical networks, how to take into account inhibitors ? 

Ancient version : simple arrow (like reactants)

New version : double arrow (like enzymes)

\end{frame}


\begin{frame}[fragile]{Why do we complain about models in Biomodels ?}

Example of wrong reductions : Mapk 27, from Markevich 2004
\bigskip
Before curation :

\begin{Verbatim}[fontsize=\scriptsize]
k1cat*MAPKK*M/Km1/(1+M/Km1+Mp/Km2)for M=[MAPKK]=>Mp.
k2cat*MAPKK*Mp/Km2/(1+M/Km1+Mp/Km2)for Mp=[MAPKK+M]=>Mpp.
k3cat*MKP3*Mpp/Km3/(1+Mpp/Km3+Mp/Km4+M/Km5)for Mpp=[MKP3+M]=>Mp.
k4cat*MKP3*Mp/Km4/(1+Mpp/Km3+Mp/Km4+M/Km5)for Mp=[MKP3+Mpp]=>M.
\end{Verbatim}

After curation :

\begin{Verbatim}[fontsize=\scriptsize]
k1cat*mapkk*m/km1/(1+m/km1+mp/km2)for m/(m,mp)=[mapkk]=>mp.
k2cat*mapkk*mp/km2/(1+m/km1+mp/km2)for mp/(m,mp)=[mapkk]=>mpp.
k3cat*mkp3*mpp/km3/(1+mpp/km3+mp/km4+m/km5)for mpp/(m,mp,mpp)=[mkp3]=>mp.
k4cat*mkp3*mp/km4/(1+mpp/km3+mp/km4+m/km5)for mp/(m,mp,mpp)=[mkp3]=>m.
0 for _=>mkp3+mapkk.
\end{Verbatim}


\end{frame}

\begin{frame}[fragile]{Why do we complain about models in Biomodels ?}

Example Mapk 27, from Markevich 2004
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{center}
\begin{figure}
\includegraphics[width=\textwidth]{images/Markevich_fig3.png}
\caption{Figure expected (from Markevich's paper)}
\end{figure}
\end{center}
\end{column}
\begin{column}{0.5\textwidth}
	\begin{figure}
\includegraphics[width=\textwidth]{images/dose_response_27_new.png}
\caption{Obtained dose-response curve with model 27}
\end{figure}
\end{column}
\end{columns}
\end{frame}

\begin{frame}{Why do we complain about models in Biomodels ?}

Parsing problems for Calcium oscillations : pow(a,n) or flat graphs

\end{frame}

\frame{

\begin{center}
	
	\textbf{Thank you for your attention.}
	
\end{center}

}

% Variants of Michaelis-Menten with a 2-ligan sites enzyme

\end{document}
