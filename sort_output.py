#!/usr/bin/env python3
"""
Read output of search_sepi.sh and order the lines
"""

import argparse
import logging
import re

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.WARNING)

mapk=['009','010','011','026','027','028','029','030','031','049','146']
circ=['021','022','034','055','073','074','078','083','089','170','171']
ca=['039','043','044','045','058','098','115','117','122','145','166']
cell=['007','008','056','109','111','144','168','169','196']


def parseArguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("inputFile", help="inputFile", type=str)
    parser.add_argument("outputFile", help="outputFile", type=str)
    args = parser.parse_args()
    return args

def read_ouput(inputFile):
    """Read a file and return a dict with line ordered
    Input:
        inputFile (str): input file name
    Output:
        (dict): dict representing sepi between models
    """
    logging.debug('Start reading inputFile')
    dico_model = {}
    dico_model["timedout"] = {}
    dico_model["timedout"]["intra"] = []
    dico_model["timedout"]["inter"] = []
    dico_model["nosepi"] = {}
    dico_model["nosepi"]["intra"] = []
    dico_model["nosepi"]["inter"] = []
    dico_model["sepi"] = {}
    dico_model["sepi"]["intra"] = []
    dico_model["sepi"]["inter"] = []
    with open(inputFile,'r') as data:
        for line in data.readlines():
            try:
                m = re.match(r"BIOMD0000000(...) BIOMD0000000(...) (.*)", line)
                mA, mB, res = m.group(1), m.group(2), m.group(3)
            except AttributeError:
                continue
            if (mA in mapk or mA in mapk) and (mB in mapk or mB in mapk):
                if res == "-1":
                    if (mA in mapk and mB in mapk) or (mA in circ and mB in circ) or (mA in ca and mB in ca) or (mA in cell and mB in cell):
                        dico_model["timedout"]["intra"].append(line)
                    else:
                        dico_model["timedout"]["inter"].append(line)
                elif res == "0":
                    if (mA in mapk and mB in mapk) or (mA in circ and mB in circ) or (mA in ca and mB in ca) or (mA in cell and mB in cell):
                        dico_model["nosepi"]["intra"].append(line)
                    else:
                        dico_model["nosepi"]["inter"].append(line)
                else:
                    if (mA in mapk and mB in mapk) or (mA in circ and mB in circ) or (mA in ca and mB in ca) or (mA in cell and mB in cell):
                        dico_model["sepi"]["intra"].append(line)
                    else:
                        dico_model["sepi"]["inter"].append(line)
    logging.debug('Dict constructed')
    return dico_model


def printer(dico_model,outputFile):
    """Takes a dict object and write it to a file
    Input:
        dico_model (dict): dict representing sepi between models
        outputFile (str): file in which to write the lines
    """
    logging.debug('Print dict in outputFile')
    with open(outputFile,'w') as OUTPUT:
        print('--- intra',file=OUTPUT)
        for line in dico_model["nosepi"]["intra"]:
            print('{}'.format(line),file=OUTPUT, end='')
        print('--- inter',file=OUTPUT)
        for line in dico_model["nosepi"]["inter"]:
            print('{}'.format(line),file=OUTPUT, end='')
        print('--- intra',file=OUTPUT)
        for line in dico_model["timedout"]["intra"]:
            print('{}'.format(line),file=OUTPUT, end='')
        print('--- inter',file=OUTPUT)
        for line in dico_model["timedout"]["inter"]:
            print('{}'.format(line),file=OUTPUT, end='')
        print('--- intra',file=OUTPUT)
        for line in dico_model["sepi"]["intra"]:
            print('{}'.format(line),file=OUTPUT, end='')
        print('--- inter',file=OUTPUT)
        for line in dico_model["sepi"]["inter"]:
            print('{}'.format(line),file=OUTPUT, end='')

if __name__ == "__main__":
    logging.debug('Start program')
    args = parseArguments()
    dico_model = read_ouput(args.inputFile)
    printer(dico_model,args.outputFile)
