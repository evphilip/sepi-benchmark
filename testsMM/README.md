Expérimentations sur des motifs de fusion de 2 Michaelis-Menten

# Fichiers .bc : 
modèles de ces motifs sous forme complète (0), sans la décomplexation (1) et réduite (2)

Les deux Michaelis-Menten sont notés a et b, et leurs espèces aS,aC,aE,aP et bS,bC,bE, bP. 
Quand deux espèces sont fusionnées, leurs noms sont concaténés, en commençant par a (ex : aEbS).
Le nom des fusions est composé de groupes de deux lettres, la première est une espèce de a, la seconde l'espèce de b avec laquelle elle est fusionnée.
On ne répète pas les fusions symétriques obtenues en échangeant a et b.
- fusion selon une seule espèce :
	- SS
	- EE
	- PP
	- PE
	- PS
	- ES
- fusion selon deux espèces :
	- SSEE
	- SSPP
	- EEPP
	- SSPE
	- EEPS
	- PPES
	- PEEP
	- SPPS
	- ESSE
	- PESP
	- PEES
	- PSSE
- fusion selon 3 espèces :
	- SPEEPS
	- SEESPP
	- SSEPPE
	- SEEPPS (isomorphe avec SPESPE : non réécrit sauf avec d'anciesn tests)
	- SSEEPP

Liste de tous les modèles : 
SS_0 SS_1 SS_2 EE_0 EE_1 EE_2 PP_0 PP_1 PP_2 PE_0 PE_1 PE_2 PS_0 PS_1 PS_2 ES_0 ES_1 ES_2 SSEE_0 SSEE_1 SSEE_2 SSPP_0 SSPP_1 SSPP_2 EEPP_0 EEPP_1 EEPP_2 SSPE_0 SSPE_1 SSPE_2 EEPS_0 EEPS_1 EEPS_2 PPES_0 PPES_1 PPES_2 PEEP_0 PEEP_1 PEEP_2 SPPS_0 SPPS_1 SPPS_2 ESSE_0 ESSE_1 ESSE_2 PESP_0 PESP_1 PESP_2 PEES_0 PEES_1 PEES_2 PSSE_0 PSSE_1 PSSE_2 SPEEPS_0 SPEEPS_1 SPEEPS_2 SEESPP_0 SEESPP_1 SEESPP_2 SSEPPE_0 SSEPPE_1 SSEPPE_2 SEEPPS_0 SEEPPS_1 SEEPPS_2 SSEEPP_0 SSEEPP_1 SSEEPP_2

Listes des modèles 0 et 1 :
SS_0 SS_1 EE_0 EE_1 PP_0 PP_1 PE_0 PE_1 PS_0 PS_1 ES_0 ES_1 SSEE_0 SSEE_1 SSPP_0 SSPP_1 EEPP_0 EEPP_1 SSPE_0 SSPE_1 EEPS_0 EEPS_1 PPES_0 PPES_1 PEEP_0 PEEP_1 SPPS_0 SPPS_1 ESSE_0 ESSE_1 PESP_0 PESP_1 PEES_0 PEES_1 PSSE_0 PSSE_1 SPEEPS_0 SPEEPS_1 SEESPP_0 SEESPP_1 SSEPPE_0 SSEPPE_1 SEEPPS_0 SEEPPS_1 SSEEPP_0 SSEEPP_1

Critères d'évaluation : idéalement on ne voudrait retrouver que les réductions de motifs Michaelis-Menten (théoriquement entre 1 et 9 SEPIs)

	Même si des SEPIs ne sont pas souhaités, on peut aussi vouloir diminuer le nombre de SEPIs entre chaque paire.

# Tables .ods :
tous les SEPIs entre 2 motifs (le plus souvent entre les formes 0 et 2, sinon j'indique MM12 quand on regarde entre les formes 1 et 2)

# Scripts .py ou .sh

- table_reductions.py : pour calculer tous les sepis entre 2 motifs et créer une table excel en sortie

	commande : python3 table_reductions.py modele1.bc modele2.bc outputFile.ods

- search_sepi_MM.sh : pour tester l'existence (et le nombre) de sepis pour toutes les paires de l'ensemble de tous les motifs

- draw_graph.py : pour tracer l'image .jpg du graphe correspondant au fichier .txt produit par search_sepi_MM.sh
  modifié par rapport à la version du git pour autoriser de tracer le graphe même quand une espèce n'est pas présente dans la colonne de gauche

- count_sepi.py : statistiques pour remplir tableau de résultats

# Certains tests (.txt et .jpg) :

- search_sepi_MM : toutes les paires de motifs
- search_sepi_MM_merge_restriction : avec la restriction (SEPI*) implémentée par Orianne (extrêmement long à calculer : plusieurs jours)
- search_sepi_MM_neigh : avec la restriction "neigh"
- sepis_2MM_i : nombre de sepis entre les paires de motifs qui sont des fusions de 2 MM à i espèces
- time_MMij_neigh : temps pour calculer tous les SEPIs entre les états i et j avec neigh
- test_maxbot : recherche de mauvais SEPIs donnés par max_bottom

# Tests pour l'instant en échec :

- search_sepi_MM_merge_restriction_all : options merge_restriction + all_reductions pour calculer le nombre de SEPIs (pour l'instant ne trouve pas certains SEPIs* alors qu'ils existent)
- exemple de paire de graphes qui donne cette erreur quand on met ces deux options : MM_SSPP_0 et MM_SSPP_2 (calcul de 16 SEPIs puis "Error : out of local stack", sachant que sans merge_restriction on trouve 18 SEPIs entre ces deux graphes), idem pour MM_SPPS_0 et MM_SPPS_2 (calcul de 16 SEPIs, 40 sont possibles sans merge_restriction)
- ERROR: sepi_sat:solve_sat/8: Arguments are not sufficiently instantiated
  quand on cherche tous les SEPIs qui maximisent bottom entre MM_EEPS_0.bc et MM_SPPS_2.bc (mais les 4 SEPIs sont bien affichés à l'écran, ce sont les seuls et ils maximisent tous bottom)
