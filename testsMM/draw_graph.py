#!/usr/bin/env python3
"""
Read output of search_sepi.sh and create the corresponding graph
"""

import argparse
import logging
import os
import re

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', level=logging.WARNING)


def parseArguments():
    parser = argparse.ArgumentParser()
    # Positional mandatory arguments
    parser.add_argument("inputFile", help="inputFile", type=str)
    parser.add_argument("outputFile", help="outputFile", type=str)
    # Optional arguments
    parser.add_argument("-t", dest='timeout', help="draw timeout", default=False)
    parser.add_argument("-c", dest='color', help="distinct colors for each class", default=False)
    parser.add_argument("-s", dest='sepis', help="write sepi count", default=False)
    args = parser.parse_args()
    return args


def read_ouput(inputFile):
    """Read a file and return a dict representing sepi between models
    Input:
        inputFile (str): input file name
    Output:
        (dict): dict representing sepi between models
    """
    logging.debug('Start to construct dict from inputFile')
    dico_model = {}
    with open(inputFile,'r') as data:
        for line in data.readlines():
            try:
                m = re.match(r"(.*) (.*) (-*[0-9]*).*", line)
                mA, mB, res = m.group(1), m.group(2), m.group(3)
                if mA not in dico_model:
                    dico_model[mA] = {}
                    dico_model[mA]["timedout"] = []
                    dico_model[mA]["nbsepi"] = []
            except AttributeError:
                logging.debug('line {}'.format(line))
                logging.debug('error')
            if res == "-1":
                dico_model[mA]["timedout"].append(mB)
            elif res == "0":
                pass
            else:
                dico_model[mA]["nbsepi"].append([mB, res])
    logging.debug('Dict constructed')
    logging.debug('dico_model:{}'.format(dico_model))
    return dico_model


def is_child(dico_model,modelA,modelB):
    """Check if modelA is a descendant of modelB in the dico_model hierarchy
    Input:
        dico_model (dict): dict representing sepi between models
        modelA (str): name of first model
        modelB (str): name of second model
    Ouput:
        (bool): true if modelA is child of modelB, false otherwise
    """
    seen = []
    to_see = [modelB]
    while to_see:
        try:
            for model_list in dico_model[to_see[0]]["nbsepi"]:
                model = model_list[0]
                if model == modelA: return True
                if model not in (seen+to_see):
                    to_see.append(model)
            seen.append(to_see[0])
            del to_see[0]
        except KeyError: 
            logging.debug('Model {} '.format(to_see[0]))
            seen.append(to_see[0])
            del to_see[0]
    return False


def curate(dico_model):
    """Curate in place a dico_model object
    Input:
        dico_model (dict): dict representing sepi between models
    """
    logging.debug('Remove transitive sepis')
    for modelA in sorted(dico_model):
        logging.debug('Looking for child of:{}'.format(modelA))
        for model_listB in dico_model[modelA]["nbsepi"]:
            modelB = model_listB[0]
            logging.debug('In particular:{}'.format(modelB))
            for model_listC in list(dico_model[modelA]["nbsepi"]):
                modelC = model_listC[0]
                is_BC = (modelB == modelC)
                child_CB = is_child(dico_model,modelC,modelB)
                logging.debug('modelB:{} modelC:{} child_CB:{}'.format(modelB, modelC, child_CB))
                if (not is_BC) and child_CB:
                    dico_model[modelA]["nbsepi"].remove(model_listC)
    logging.debug('Done removing transitive sepis')


def printer(dico_model,outputFile,timeout,color,sepis):
    """Takes a dict object and print a graph.dot file
    Input:
        dico_model (dict): dict representing sepi between models
        outputFile (str): file in which to write the graph
    """
    logging.debug('Print dict as graph in outputFile')
    with open(outputFile,'w') as OUTPUT:
        logging.debug('create dot file')
        print('digraph G {',file=OUTPUT)
        for modelA in sorted(dico_model):
            for model_list in dico_model[modelA]["nbsepi"]:
                if sepis:
                    print('\t{} -> {} [ label="{}"];'.format(modelA, model_list[0], model_list[1]),file=OUTPUT)
                else:
                    print('\t{} -> {} ;'.format(modelA, model_list[0]),file=OUTPUT)
            if timeout:
                for modelC in dico_model[modelA]["timedout"]:
                    print("\t{} -> {} [style=dashed, color=red];".format(modelA, modelC),file=OUTPUT)
        print('}',file=OUTPUT)

if __name__ == "__main__":
    logging.debug('Start program')
    args = parseArguments()
    dico_model = read_ouput(args.inputFile)
    logging.debug('dico_model:{}'.format(dico_model))
    curate(dico_model)
    logging.debug('dico_model:{}'.format(dico_model))
    printer(dico_model,"tempo.dot",args.timeout, args.color,args.sepis)
    os.system("dot tempo.dot -T jpg > {0}".format(args.outputFile))
    os.system("rm tempo.dot")
