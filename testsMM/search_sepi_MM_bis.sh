#!/bin/bash
#set -e

# function to check if there is a sepi between two graphs
# output: 0 if not sepi found, 1 if sepi found, -1 if timed out
test_once () {
    OUTPUT=$(echo "search_reduction('MM_$filename1.bc', 'MM_$filename2.bc', merge_restriction: $merge_restriction, timeout: $timeout, all_reductions: $all_reductions, max_nb_reductions: $max_nb_reductions, extremal_sepi: $extremal_sepi)." | biocham)
    last=$(echo "$OUTPUT" | tail -n 1)
    timedout=$(echo "$OUTPUT" | tail -n 3)
    if [ "$last" == "no sepi found" ] # no sepi found (without even using sat solver)
    then
        resbiocham="0"
    else
        if [[ $timedout == *"timed out"* ]] # timed out
        then
            resbiocham="-1"
        else # read number of reductions, 0 if no sepi
            regex='.* (_*[0-9]*)'
            [[ $last =~ $regex ]]
            resbiocham=${BASH_REMATCH[1]}
        fi
    fi
    if [[ $OUTPUT == *"timed out"* ]]
    then 
        timed="timedout"
    else
        timed="no timedout"
    fi
}

# function to find the number of reductions once it is known that there is a sepi between two graphs
# do not use Biocham
# ouput: the number of reductions (between 1 and $sepilimit)
all_reductions () {
    SOLUTIONS=0
    cp /tmp/in.sat /tmp/tmpsat
    while : ; do
        timeout $timeout glucose -verb=0 /tmp/tmpsat /tmp/tmpout > /tmp/tmpmsg 2> /tmp/tmpmsg
        exit_status=$?
        if [[ $exit_status -eq 124 ]]; then
            break
        fi
        res="$(tail -n 1 /tmp/tmpout)"
        if [ "$res" == "UNSAT" ]; then
            break
        fi
        SOLUTIONS=$((SOLUTIONS + 1))
        if [ "$debug" == "yes" ]; then
            echo "For now $SOLUTIONS solutions."
        fi
        if [ "$SOLUTIONS" -ge "$sepilimit" ]; then
            break
        fi
        tail -n 1 /tmp/tmpout | # write the negation of the solution found
            awk '{
                for(i=1;i<NF;++i) { $i = -$i }
                print
            }' >> /tmp/tmpsat
    done
    rm -f /tmp/tmpsat
    rm -f /tmp/tmpout
    rm -f /tmp/tmpmsg
}

# function to check for sepi between a pair of model
search_sepi () {
    now=$(date +"%T")
    echo "$now test between $filename1 $filename2"
    test_once # set variable $resbiocham and $timed
    echo "$filename1 $filename2 $resbiocham" >> "$output"
    echo "$resbiocham"
}

test_all () {
    for i in $classall ; do
        for j in $classall ; do
            if [ "$i" != "$j" ]
            then
                filename1=$i
                filename2=$j
                search_sepi
            fi
        done
    done
}

test_pair () {
    for p in $classpair ; do
        IFS='-' read -ra ADDR <<< "$p"
        set -- "${ADDR[@]}"
        filename1=$1
        filename2=$2
        search_sepi
    done
}

# variables to set manually
classname="all"
classall="SS_0 SS_1 SS_2 EE_0 EE_1 EE_2 PP_0 PP_1 PP_2 PE_0 PE_1 PE_2 PS_0 PS_1 PS_2 ES_0 ES_1 ES_2 SSEE_0 SSEE_1 SSEE_2 SSPP_0 SSPP_1 SSPP_2 EEPP_0 EEPP_1 EEPP_2 SSPE_0 SSPE_1 SSPE_2 EEPS_0 EEPS_1 EEPS_2 PPES_0 PPES_1 PPES_2 PEEP_0 PEEP_1 PEEP_2 SPPS_0 SPPS_1 SPPS_2 ESSE_0 ESSE_1 ESSE_2 PESP_0 PESP_1 PESP_2 PEES_0 PEES_1 PEES_2 PSSE_0 PSSE_1 PSSE_2 SPEEPS_0 SPEEPS_1 SPEEPS_2 SEESPP_0 SEESPP_1 SEESPP_2 SSEPPE_0 SSEPPE_1 SSEPPE_2 SEEPPS_0 SEEPPS_1 SEEPPS_2 SSEEPP_0 SSEEPP_1 SSEEPP_2"
classpair=""
merge_restriction="neighbours"
timeout="1200"
all_reductions="yes"
max_nb_reductions="200"
extremal_sepi="no"
debug="yes"
output="search_sepi_2MM_all_neigh.txt"
# end of variables to set manually
cd $classname
now=$(date +"%T")

test_all
#test_pair