#!/usr/bin/env python3
"""
Create a table for all reductions between two graphs
"""

import csv
import sys
from subprocess import PIPE, run
from tabulate import tabulate

def all_reductions(g1,g2):
    """Search all sepis between two graphs and return them in a list
    Input:
        g1 (str): file containing the first graph
        g2 (str): file containing the second graph
    Ouput:
        (list): all sepis between the graphs
    """
    command = "option(timeout: 180). option(all_reductions: yes). option(reduction:normal). option(merge_restriction:yes). search_reduction('{}','{}'). quit.".format(g1,g2)
    result = run("biocham",
        input=command,
        stdout=PIPE, stderr=PIPE,
        universal_newlines=True)
    sepis = result.stdout.split('sepi\n')
    sepis.pop(0)
    return sepis

def read_ouput(sepis, outputFile):
    """Transform a list of sepi in a more readable table
    Input:
        sepis (list): all sepis between the graphs
        outputFile (str): output file name
    """
    first = sepis.pop(0)
    operations = first.split('\n')
    title = ["sepis"]
    transf = [0]
    for operation in operations:
        try:
            title.append(operation.split(' -> ')[0])
            transf.append(operation.split(' -> ')[1])
        except IndexError:
            pass
    table = [title, transf]
    for i, sepi in enumerate(sepis):
        transf = [i+1]
        operations = sepi.split('\n')
        for operation in operations:
            try:
                transf.append(operation.split(' -> ')[1])
            except IndexError:
                pass
        table.append(transf)
    with open(outputFile, "w") as f:
        writer = csv.writer(f)
        writer.writerows(table)
    print(tabulate(table, headers=table.pop(0), tablefmt='orgtbl'))

if __name__ == "__main__":
    g1 = sys.argv[1]
    g2 = sys.argv[2]
    outputFile = sys.argv[3]
    sepis = all_reductions(g1,g2)
    read_ouput(sepis, outputFile)
